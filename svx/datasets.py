
import os


class Dataset(object):

    def __init__(self, dataset_name):
        self.base_path = '../data/' + dataset_name

    def get_images_path(self, video):
        return os.path.join(self.base_path, 'frames', video, 'png')

    def get_edges_path(self, video):
        return os.path.join(self.base_path, 'edges', video)

    def get_flow_path(self, video, direction):
        return os.path.join(self.base_path, 'flow', direction, video)

    def get_segmentation_directory(self, video):
        return os.path.join(self.base_path, 'segmentations', video)

    def get_segmentation_paths(self, video, level):
        d = os.path.join(self.get_segmentation_directory(video), str(level))
        return sorted([os.path.join(d, f) for f in os.listdir(d) if f.endswith('png')])

    def get_tree_path(self, video):
        return os.path.join(self.base_path, 'hierarchies', '%s.npz')

    def get_graph_path(self, video, level, features):
        return os.path.join(
            self.get_segmentation_directory(video),
            str(level),
            'graph_weights_%s.txt' % '_'.join(features))

    def get_proposal_directory(self, video):
        return os.path.join(self.base_path, 'proposals', video)

    # Generic paths.
    def get_generic_images_path(self, video):
        return os.path.join(self.get_images_path(video), '%06d.png')

    def get_generic_flow_path(self, video):
        return os.path.join(self.get_flow_path(video, 'forward'), '%06d.flo')

    def get_generic_segmentation_path(self, video, level):
        return os.path.join(self.get_segmentation_directory(video), str(level), '%06d.png')

    def parse_groundtruth(self, video):
        groundtruth_path = os.path.join(self.base_path, 'groundtruth', video + '.txt')
        with open(groundtruth_path, 'r') as ff:
            for line in ff.readlines():
                frame_nr, xpos, ypos, width, height = map(int, line.split())
                yield (
                    frame_nr,
                    xpos,
                    ypos,
                    xpos + width,
                    ypos + height,
                )


DATASETS = {
    'ucf_sports': Dataset('ucf_sports'),
    'kth': Dataset('kth'),
}

