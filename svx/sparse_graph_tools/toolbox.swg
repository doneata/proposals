// -*- c++ -*- 

%module toolbox;

%{
#include "sparse_graph_tools.h"
#include "graph_dist.h"
#include "from_img.h"
%}

%include <common/numpy_image.swg>

// By default, functions release Python's global lock

%exception {
  Py_BEGIN_ALLOW_THREADS
  $action
  Py_END_ALLOW_THREADS
}

%pythoncode %{
import numpy as np
%}


/**********************************************************************
* sparse graph tools
*/

%include "sparse_graph_tools.h"


%pythoncode %{

def cs_connected_components( sp ):
  assert sparse.isspmatrix_coo(sp), 'error: input must be a coo_matrix'
  assert sp.shape[0]==sp.shape[1], 'error: sparse matrix is not square'
  assert sp.dtype.name=='float32', 'error: dtype has to be float32'
  n_nodes = sp.shape[0]
  labels = np.empty(n_nodes,dtype=np.int32)
  n_labels = find_connected_components(sp, labels)
  return n_labels, labels


def minimum_spanning_tree( g ):
  assert sparse.isspmatrix_coo(g), 'error: input must be a coo_matrix'
  assert g.shape[0]==g.shape[1], 'error: sparse matrix is not square'
  assert g.dtype.name=='float32', 'error: dtype has to be float32'
  n_nodes = g.shape[0]
  
  # prepare res
  res = sparse.coo_matrix(g.shape,dtype=np.float32)
  res.row = np.empty(n_nodes-1,dtype=np.int32)
  res.col = np.empty(n_nodes-1,dtype=np.int32)
  res.data = np.empty(n_nodes-1,dtype=np.float32)
  
  # compute MST
  g.row = g.row.ravel() # important because np.array might not be contiguous
  g.col = g.col.ravel()
  g.data = g.data.ravel()
  n_edges = _minimum_spanning_tree(g,res)
  
  # remove unused edges
  res.row = res.row[:n_edges]
  res.col = res.col[:n_edges]
  res.data = res.data[:n_edges]
  return res


def union_minimum( csr1, csr2 ):
  csr = _union_minimum( csr1, csr2 )
  return tuple_to_csr(csr)


def intersect_left( csr1, csr2, add_missing=1):
  csr = _intersect_left( csr1, csr2, add_missing )
  return tuple_to_csr(csr)

%}



/**********************************************************************
* graph distance propagation
*/

%include "graph_dist.h"


%pythoncode %{

def find_nn_graph( graph, seed, nn ):
  nnf = np.empty(nn,dtype=np.int32)
  dis = np.empty(nn,dtype=np.float32)
  n = _find_nn_graph( graph, seed, nnf, dis )
  return nnf[:n], dis[:n]


def count_neighbours( assoc, rad=1, weights=None ):
  count = np.empty(assoc.shape[0],dtype=np.float32)
  _count_neighbours( assoc.tocsr(), rad, weights, count )
  return count


def find_neighbours( assoc, seed=None, rad=1, radmin=1, nt=1 ):
  if seed!=None:
    return _find_neighbours_node( assoc, seed, radmin, rad )
  else:
    return tuple_to_csr(_find_neighbours( assoc, radmin, rad, nt ))


def max_filter( node_scores, ngh, rad=1 ):
  res = np.empty_like(node_scores)
  if rad>1: node_scores = node_scores.copy()
  for r in range(rad):
    _max_filter( ngh, node_scores, res )
    node_scores, res = res, node_scores
  return node_scores


def non_max_suppr( node_scores, ngh, rad=1 ):
  maxf = max_filter( node_scores, ngh, rad=rad )
  return where(node_scores==maxf, maxf, 0)


def shortest_path( assoc, start, end, output_path=False, weighted=True ):
  if not weighted:
    assoc = assoc.copy()
    assoc.data[:] = 1
  res = _shortest_weighted_path( assoc, start, end, output_path )
  if not output_path: res = res[0]
  return res


def shortest_dist_nhops( graph, nhops, seed=None, weighted=True, nt=1 ):
  if not weighted:
    graph = graph.copy()
    graph.data[:] = 1
  nr = graph.shape[0]
  if seed!=None:
    res = np.empty((nr,nhops), dtype=np.float32)
    _shortest_weighted_nhops_node( graph, seed, nhops, res )
  else:
    csr = _shortest_weighted_nhops( graph, nhops, nt )
    res = tuple_to_csr(csr)
  return res

%}



/**********************************************************************
* labeled image --> sparse graph
*/

%include "from_img.h"


%pythoncode %{

def ngh_labels_to_spmat( nl, labels, cost_map=None, ori_map=None, linkage='a', min_border_size=0 ):
  if cost_map==None:
    csr = _ngh_labels_to_spmat( nl, labels, min_border_size )
  elif ori_map==None:
    csr = _ngh_labels_to_spmat_weighted( nl, labels, cost_map, linkage, min_border_size )
  else:
    csr = _ngh_labels_to_spmat_weighted_ori( nl, labels, cost_map, ori_map, linkage, min_border_size )
  return tuple_to_csr(csr)

def ngh_labels_to_spmat_euc( nl, labels, desc, linkage='a', min_border_size=0, connec=4 ):
  ''' euclidean distance between neigboring descriptors '''
  csr = _ngh_labels_to_spmat_euc( nl, labels, desc, linkage, connec, min_border_size )
  return tuple_to_csr(csr)

def viz_edge_weights( edges, labels ):
  assert sparse.isspmatrix_csr(edges)
  img = np.empty(labels.shape, dtype=np.float32)
  _viz_edge_weights( edges, labels, img )
  return img

def label_image_cc( labels, min_size=0, connec=4 ):
  res = np.empty_like(labels)
  nl = _label_image_cc( labels, connec, min_size, res )
  return nl,res

%}



































