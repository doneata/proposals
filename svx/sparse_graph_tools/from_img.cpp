#include "from_img.h"
#include "std.h"

#include <vector>
#include <unordered_map>
using namespace std;

//#include "graph_dist.h"



template<typename Ti>
struct Tint_float_t {
  Ti i;
  float f;
  Tint_float_t(){}
  Tint_float_t(Ti i, float f):i(i),f(f){}
};
typedef Tint_float_t<int> int_float_t;
typedef Tint_float_t<long> long_float_t;

static int cmp_int_float ( const void* a, const void* b ) {
  return ((int_float_t*)a)->i - ((int_float_t*)b)->i;
}

static int cmp_long_float ( const void* a, const void* b ) {
  long diff = ((long_float_t*)a)->i - ((long_float_t*)b)->i;
  return (diff>0) - (diff<0);
}

int cmp_float( const void* a, const void* b ) {
  return ((*(float*)a)>(*(float*)b)) - ((*(float*)a)<(*(float*)b));
}



static inline long key( long i, long j ) {
  if(j>i) SWAP(i,j,long);   // always i<j 
  return i + (j<<32);
}
static inline int first(long i) {return int(i);}
static inline int second(long i) {return int(i >> 32);}


struct border_t {
  float accu;
  float nb;
  vector<float> values;
  
  border_t():accu(0),nb(0){}
  
  void add( float v, char linkage, float w=1 ) {
    switch(linkage) {
    case 's':
      if(!nb || accu>v) accu=v;
      break;  
    case 'm':
      if(accu<v) accu=v;
      break;  
    case 'a':
      accu += v;
      break;
    case 'd': // median
      assert(w==1);
      values.emplace_back(v);
      break;
    default:
      assert(!"error: unknown linkage");
    }
    nb+=w;
  }
  
  float get_val(char linkage) const {
    switch(linkage) {
    case 'a': return accu/nb;
    case 'd': {
      qsort((void*)values.data(),values.size(),sizeof(float),cmp_float);
      int mid = values.size()/2;
      if( values.size() % 2 )
        return (values[mid-1]+values[mid])/2;
      else
        return values[mid];
      }
    default:  return accu;
    }
  }
};

typedef unordered_map<long,border_t> ngh_t;

/* Convenient function for ngh_labels*_to_spmat
   From a dictionary of border_t, create the sparse matrix.
*/
static void ngh_to_spmat( const int nl, ngh_t& ngh, 
                          const char linkage, const int min_border_size,
                          csr_matrix* csr ) {
  
  // convert neighbours into a sparse graph
  vector<long_float_t> sorted;
  for(ngh_t::iterator it=ngh.begin(); it!=ngh.end(); ++it) {
    if( it->second.nb <= min_border_size ) continue;
    const float cost = it->second.get_val(linkage);
    sorted.emplace_back(it->first,cost);
    long symkey = (it->first>>32) + (it->first<<32);
    sorted.emplace_back(symkey,cost);  // add symmetric
  }
  // sort by (row,col) = (second,first)
  qsort(sorted.data(), sorted.size(), sizeof(long_float_t), cmp_long_float);
  
  csr->nr = nl;
  csr->nc = nl;
  assert(!csr->indptr);
  csr->indptr = NEWA(int, nl+1);
  csr->indices = NEWA(int, sorted.size());
  csr->data = NEWA(float, sorted.size());
  int n=0,r=0;
  csr->indptr[0] = 0;
  while( n<(signed)sorted.size() ) {
    int row = second(sorted[n].i);
    //assert(r<=row);
    while(r<row)  csr->indptr[++r] = n;
    csr->indices[n] = first(sorted[n].i);
    csr->data[n] = sorted[n].f;
    n++;
  }
  assert(r<nl);
  // finish row marker
  while(r<nl)
    csr->indptr[++r] = n;
}


/* Find neighboring labels and store their relation (number of edge pixels) in a sparse matrix.
    nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat( int nl, int_image* labels, int min_border_size, csr_matrix* csr ) {
  const int tx = labels->tx;
  const int ty = labels->ty;
  const int* lab = labels->pixels;
  const char linkage = 'a';
  
  ngh_t ngh;
  for(int j=1; j<ty; j++)
    for(int i=1; i<tx; i++) {
      int l0 = lab[i+j*tx];
      int l1 = lab[i-1+j*tx];
      int l2 = lab[i+(j-1)*tx];
      if( l0!=l1 ) {
        long k = key(l0,l1);
        ngh[k].add(1,linkage);
      }
      if( l0!=l2 ) {
        long k = key(l0,l2);
        ngh[k].add(1,linkage);
      }
    }
  
  ngh_to_spmat( nl, ngh, 'm', min_border_size, csr );
}



/* Find neighboring labels, compute their edge cost based on a cost map, 
   and store their relation in a sparse matrix.
     linkage = 's' (single == mininum) 'a' (average) or 'm' (maximum)
     nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat_weighted( int nl, int_image* labels, float_image* cost_map, char linkage, 
                                    int min_border_size, csr_matrix* csr ) {
  ASSERT_SAME_SIZE(labels, cost_map);
  const int tx = labels->tx;
  const int ty = labels->ty;
  const int* lab = labels->pixels;
  const float* dis = cost_map->pixels;
  
  typedef unordered_map<long,border_t> ngh_t;
  ngh_t ngh;
  for(int j=1; j<ty; j++)
    for(int i=1; i<tx; i++) {
      int l0 = lab[i+j*tx];
      int l1 = lab[i-1+j*tx];
      int l2 = lab[i+(j-1)*tx];
      if( l0!=l1 ) {
        long k = key(l0,l1);
        float d = MAX(dis[i+j*tx], dis[i-1+j*tx]);
        ngh[k].add(d,linkage);
      }
      if( l0!=l2 ) {
        long k = key(l0,l2);
        float d = MAX(dis[i+j*tx], dis[i+(j-1)*tx]);
        ngh[k].add(d,linkage);
      }
    }
  
  ngh_to_spmat( nl, ngh, linkage, min_border_size, csr );
}




/* Find neighboring labels, compute their edge cost based on a cost map, 
   and store their relation in a sparse matrix.
     linkage = 's' (single == mininum) 'a' (average) or 'm' (maximum)
     nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat_weighted_ori( int nl, int_image* labels, float_image* cost_map, float_image* ori_map, 
                                      char linkage, int min_border_size, csr_matrix* csr ) {
  ASSERT_SAME_SIZE(labels, cost_map);
  ASSERT_SAME_SIZE(labels, ori_map);
  const int tx = labels->tx;
  const int ty = labels->ty;
  const int* lab = labels->pixels;
  const float* dis = cost_map->pixels;
  const float* ori = ori_map->pixels;
  
  typedef unordered_map<long,border_t> ngh_t;
  
  // first, we estimate the orientation between each superpixels
  ngh_t xs, ys;
  for(int j=1; j<ty; j++)
    for(int i=1; i<tx; i++) {
      int l0 = lab[i+j*tx];
      int l1 = lab[i-1+j*tx];
      int l2 = lab[i+(j-1)*tx];
      if( l0!=l1 ) {
        long k = key(l0,l1);
        xs[k].add(l1<l0 ? 1 : -1,'a');
        ys[k].add(0,'a');
      }
      if( l0!=l2 ) {
        long k = key(l0,l2);
        xs[k].add(0,'a');
        ys[k].add(l2<l0 ? 1 : -1,'a');
      }
    }
  
  // now we weight the cost using the orientation
  ngh_t ngh;
  for(int j=1; j<ty; j++)
    for(int i=1; i<tx; i++) {
      int l0 = lab[i+j*tx];
      int l1 = lab[i-1+j*tx];
      int l2 = lab[i+(j-1)*tx];
      if( l0!=l1 ) {
        long k = key(l0,l1);
        float o1 = ori[i+j*tx];
        float o2 = ori[i-1+j*tx];
        float x = xs[k].get_val('a');
        float y = ys[k].get_val('a');
        float norm = 1e-8 + sqrt(x*x + y*y);
        x/=norm; y/=norm;
        float w1 = pow2( cos(o1)*x + sin(o1)*y );
        float w2 = pow2( cos(o2)*x + sin(o2)*y );
        float d = MAX(w1*dis[i+j*tx], w2*dis[i-1+j*tx]);
        ngh[k].add(d,linkage);
      }
      if( l0!=l2 ) {
        long k = key(l0,l2);
        float o1 = ori[i+j*tx];
        float o2 = ori[i+(j-2)*tx];
        float x = xs[k].get_val('a');
        float y = ys[k].get_val('a');
        float norm = 1e-8 + sqrt(x*x + y*y);
        x/=norm; y/=norm;
        float w1 = pow2( cos(o1)*x + sin(o1)*y );
        float w2 = pow2( cos(o2)*x + sin(o2)*y );
        float d = MAX(w1*dis[i+j*tx], w2*dis[i+(j-1)*tx]);
        ngh[k].add(d,linkage);
      }
    }
  
  ngh_to_spmat( nl, ngh, linkage, min_border_size, csr );
}




/* Find neighboring labels, based on euclidean distance between neigboring pixel
   descriptors, and store their relation in a sparse matrix.
     linkage = 's' (single == mininum) 'a' (average) or 'm' (maximum)
     nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat_euc( int nl, int_image* labels, float_cube* desc, char linkage, 
                               int connec, int min_border_size, csr_matrix* csr ) {
  ASSERT_SAME_SIZE(labels, desc);
  assert(connec==4 || connec==8);
  
  const int tx = labels->tx;
  const int ty = labels->ty;
  const int tz = desc->tz;
  const int* lab = labels->pixels;
  const float* des = desc->pixels;
  
  typedef unordered_map<long,border_t> ngh_t;
  ngh_t ngh;
  for(int j=1; j<ty; j++)
    for(int i=1; i<tx; i++) {
      int l0 = lab[i+j*tx];
      int l1 = lab[i-1+j*tx];
      int l2 = lab[i+(j-1)*tx];
      if( l0!=l1 ) {
        long k = key(l0,l1);
        float dis = 0;
        for(int k=0; k<tz; k++) 
          dis += pow2(des[k+(i+j*tx)*tz] - des[k+(i-1+j*tx)*tz]);
        dis = sqrt(dis);
        ngh[k].add(dis,linkage);
      }
      if( l0!=l2 ) {
        long k = key(l0,l2);
        float dis = 0;
        for(int k=0; k<tz; k++) 
          dis += pow2(des[k+(i+j*tx)*tz] - des[k+(i+(j-1)*tx)*tz]);
        dis = sqrt(dis);
        ngh[k].add(dis,linkage);
      }
      if(connec==4) continue;
      int l3 = lab[i-1+(j-1)*tx];
      if( l0!=l3 ) {
        long k = key(l0,l3);
        float dis = 0;
        for(int k=0; k<tz; k++) 
          dis += pow2(des[k+(i+j*tx)*tz] - des[k+(i-1+(j-1)*tx)*tz]);
        dis = sqrt(dis);
        ngh[k].add(dis,linkage);
      }
      if( l1!=l2 ) {
        long k = key(l1,l2);
        float dis = 0;
        for(int k=0; k<tz; k++) 
          dis += pow2(des[k+(i-1+j*tx)*tz] - des[k+(i+(j-1)*tx)*tz]);
        dis = sqrt(dis);
        ngh[k].add(dis,linkage);
      }
    }
  
  ngh_to_spmat( nl, ngh, linkage, min_border_size, csr );
}






/* visualize distances between neighboring nodes
*/
void _viz_edge_weights( const csr_matrix* edges, int_image* labels, float_image* _img ) {
  ASSERT_SAME_SIZE(labels,_img);
  const int tx = _img->tx;
  const int ty = _img->ty;
  
  typedef unordered_map<long,float> edges_t;
  edges_t ngh;
  
  // transfer csr to map
  for(int i=0; i<edges->nr; i++) {
    for(int j=edges->indptr[i]; j<edges->indptr[i+1]; j++)
      ngh.emplace(key(i,edges->indices[j]),edges->data[j]);
  }
  
  // transfer to image
  float* img = _img->pixels;
  const int* lab = labels->pixels;
  memset(img,0,tx*ty*sizeof(float));
  for(int j=1; j<ty; j++)
    for(int i=1; i<tx; i++) {
      int l0 = lab[i+j*tx];
      int l1 = lab[i-1+j*tx];
      int l2 = lab[i+(j-1)*tx];
      if( l0!=l1 ) {
        long k = key(l0,l1);
        img[i-1+j*tx] = img[i+j*tx] = ngh[k];
      }
      if( l0!=l2 ) {
        long k = key(l0,l2);
        img[i+j*tx] = img[i+(j-1)*tx] = ngh[k];
      }
    }
}



/* Break down a labelled image into connected components.
   Each label == 1 region, which can be further broken down into smaller components.
   
    connec = {4,8}
    min_size = minimum size for a connected components (if larger, connect to any neighbor)
*/
int _label_image_cc( int_image* _labels, int connec, int min_size, int_image* res ) {
  ASSERT_SAME_SIZE( _labels, res );
  
  const int tx = _labels->tx;
  const int ty = _labels->ty;
  
  const int* labels = _labels->pixels;
  int* nlabels = res->pixels;
  memset(nlabels,0xFF,tx*ty*sizeof(int));  // init to -1
  
  int* xvec = NEWA(int,tx*ty);
  int* yvec = NEWA(int,tx*ty);
  
  const int dx[] = {-1,  0,  1,  0, -1, -1,  1, 1};
  const int dy[] = { 0, -1,  0,  1, -1,  1, -1, 1};
  
  int n_label=0, start=0, adjlabel=0;
  
  for(int j=0; j<ty; j++)  {
    for(int i=0; i<tx; i++,start++) {
        if( nlabels[start] < 0 )
        {
          // start a new segment
          nlabels[start] = n_label;
          int old_label = labels[start];
          
          // Quickly find an adjacent label for use later if needed
          if( min_size > 1 ) {  // useless otherwise
            for(int n=0; n<connec; n++) {
              int x = xvec[0] + dx[n];
              int y = yvec[0] + dy[n];
              if( x>=0 && y>=0 && x<tx && y<ty ) {
                int nindex = y*tx + x;
                if(nlabels[nindex] >= 0) 
                  adjlabel = nlabels[nindex];
              }
            }
          }
          
          // find connected neigbors
          xvec[0] = i;
          yvec[0] = j;
          int count=1;
          for(int c=0; c<count; c++) {
            for(int n=0; n<connec; n++) {
              
              // neighbor pixel
              int x = xvec[c] + dx[n];
              int y = yvec[c] + dy[n];
              
              if( x>=0 && y>=0 && x<tx && y<ty ) {
                int nindex = y*tx + x;
                if( nlabels[nindex]<0 &&  // not yet labelled
                    labels[nindex]==old_label ) { // same label than start
                  xvec[count] = x;
                  yvec[count] = y;
                  nlabels[nindex] = n_label;
                  count++;
                }
              }
            }
          }
          
          if(count < min_size) {
            // if two small, attach to neighbor label
            for(int c=0; c<count; c++) {
              int ind = yvec[c]*tx+xvec[c];
              nlabels[ind] = adjlabel;
            }
            n_label--;
          }
          
          n_label++;
        }
    }
  }
  
  free(xvec);
  free(yvec);
  
  return n_label;
}

































