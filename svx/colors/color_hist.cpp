#include "color_hist.h"
#include "std.h"
#include <math.h>
#include <vector>
#include <unordered_map>
using namespace std;


/* Compute a color histogram based for each superpixel
*/
void _compute_color_hist( float_cube* lab, int_image* labels, const int nl, int L_bin, int ab_bin, csr_matrix* res_out ) {
  ASSERT_SAME_SIZE(lab,labels);
  assert(lab->tz==3);
  assert(L_bin>0 && ab_bin>0);
  
  typedef unordered_map<int,float> color_hist_t;
  vector<color_hist_t> histograms(nl);
  vector<int> count(nl);
  
  const int npix = IMG_SIZE(lab);
  for(int i=0; i<npix; i++) {
    const int l = labels->pixels[i];
    count[l]++; // to normalize rows
    auto& hist = histograms[l];
    
    float* p = lab->pixels+3*i;
    
    // L range is [0,100]
    // a,b range is cropped to [-50,50] 
    float fL = (L_bin*p[0]/100.001); 
    float fa = (ab_bin*(p[1]+50)/100.001);
    float fb = (ab_bin*(p[2]+50)/100.001);
    fL = bound(0,fL,L_bin-1.0001);
    fa = bound(0,fa,ab_bin-1.0001);
    fb = bound(0,fb,ab_bin-1.0001);
    
    int L = int(fL);  fL-=L;
    int a = int(fa);  fa-=a;
    int b = int(fb);  fb-=b;
    
    #define BIN(L,a,b)  ((L)*ab_bin*ab_bin + (a)*ab_bin + (b))
    
    // soft-assignement with bilinear interpolation
    hist[BIN(L  ,a  ,b  )] += (1-fL)*(1-fa)*(1-fb);
    hist[BIN(L  ,a  ,b+1)] += (1-fL)*(1-fa)*(  fb);
    hist[BIN(L  ,a+1,b  )] += (1-fL)*(  fa)*(1-fb);
    hist[BIN(L  ,a+1,b+1)] += (1-fL)*(  fa)*(  fb);
    hist[BIN(L+1,a  ,b  )] += (  fL)*(1-fa)*(1-fb);
    hist[BIN(L+1,a  ,b+1)] += (  fL)*(1-fa)*(  fb);
    hist[BIN(L+1,a+1,b  )] += (  fL)*(  fa)*(1-fb);
    hist[BIN(L+1,a+1,b+1)] += (  fL)*(  fa)*(  fb);
  }
  
  // convert to sparse matrix
  vector<int> indptr;
  vector<int> columns;
  vector<float> data;
  for(int l=0; l<nl; l++) {
    indptr.emplace_back((int)data.size());
    auto& hist = histograms[l];
    for(auto it=hist.begin(); it!=hist.end(); it++) {
      columns.emplace_back(it->first);
      data.emplace_back(it->second/float(count[l]));
    }
  }
  
  indptr.emplace_back((int)data.size());
  assert(!res_out->data);
  res_out->nr = nl;
  res_out->nc = L_bin*ab_bin*ab_bin;
  #define get_ptr(vec)  vec.data(); memset(&vec,0,sizeof(vec))
  res_out->indptr = get_ptr(indptr);
  res_out->indices = get_ptr(columns);
  res_out->data = get_ptr(data);
}


inline static float compare_sparse_hist( const csr_matrix* hist, int u, int v, int dist_type ) {
  int i=hist->indptr[u], endi=hist->indptr[u+1];
  int j=hist->indptr[v], endj=hist->indptr[v+1];
  float res = 0;
  while(i<endi || j<endj) {
    float vi=0, vj=0;
    if( j>=endj || (i<endi && hist->indices[i] < hist->indices[j]) )
      vi = hist->data[i++];
    else if( i>=endi || (j<endj && hist->indices[i] > hist->indices[j]) )
      vj = hist->data[j++];
    else {
      vi = hist->data[i++];
      vj = hist->data[j++];
    }
    switch( dist_type ) {
      case 1: res += fabs( vi - vj ); break;
      case 2: res += pow2( vi - vj ); break;
      case 12: res += pow2( vi - vj )/( 1e-16 + vi + vj ); break;
      case 0: res += MIN( vi, vj ); break;
      default:  assert(!"error: not implemented or invalid dist_type");
    }
  }
  if(dist_type==0)  res=1-res;  // 1-intersection
  assert(res==res);
  return res;
}


/* Compare color histograms of neighboring superpixels.
  dist_type = 1:'l1', 2:'l2', 0:'intersection', 12:'chi2'
*/
void compare_color_hist( csr_matrix* ngh, const csr_matrix* hist, const int dist_type ) {
  const int ns = hist->nr;
  assert(ngh->nr==ns && ngh->nc==ns);
  
  for(int i=0; i<ns; i++)
    for(int j=ngh->indptr[i]; j<ngh->indptr[i+1]; j++)
      ngh->data[j] = compare_sparse_hist(hist,i,ngh->indices[j],dist_type);
}


/* Compute a flow histogram based for each superpixel
  flow_pow = 0.2
  ninth = additional constant dimension, defined per-pixel
*/
void _compute_flow_hist( float_cube* flow, int_image* labels, const int nl, 
                         float flow_pow, float ninth, float_image* res ) {
  ASSERT_SAME_SIZE(flow,labels);
  ASSERT_IMG_ZEROS(res);
  assert(res->ty==nl);
  assert(flow->tz==2);
  const int o_bin = res->tx-1;
  assert(o_bin>0);
  
  vector<int> count(nl);
  
  const int npix = IMG_SIZE(flow);
  for(int i=0; i<npix; i++) {
    const int l = labels->pixels[i];
    count[l]++; // to normalize rows
    
    float* hist = res->pixels + l*res->tx;
    
    float* p = flow->pixels+2*i;
    float norm = pow(p[0]*p[0]+p[1]*p[1], flow_pow);
    float ori = atan2(p[1],p[0]+1e-16);
    float fo = (o_bin*(ori+M_PI)/(2.00001*M_PI));
    int o = int(fo);
    fo = fo-o;
    assert(0<=o && o<=o_bin);
    hist[o] += (1-fo)*norm;
    hist[(o+1)%o_bin] += fo*norm;
  }
  
  // normalize rows
  for(int l=0; l<nl; l++) {
    float* hist = res->pixels + l*res->tx;
    hist[o_bin] = ninth*count[l];
    
    float sum = 1e-16;
    for(int j=0; j<=o_bin; j++)  
      sum += hist[j];
    
    for(int j=0; j<=o_bin; j++) { 
      hist[j] /= sum;
      assert(hist[j]==hist[j]);
    }
  }
}



inline static float compare_hist( const float_image* hist, int u, int v, int dist_type ) {
  float* vu = hist->pixels + u*hist->tx;
  float* vv = hist->pixels + v*hist->tx;
  float res=0;
  for(int i=0; i<hist->tx; i++) {
    switch( dist_type ) {
      case 1: res += fabs( vu[i] - vv[i] ); break;
      case 2: res += pow2( vu[i] - vv[i] ); break;
      case 12: res += pow2( vu[i] - vv[i] )/( 1e-16 + vu[i] + vv[i] ); break;
      case 0: res += MIN( vu[i], vv[i] ); break;
      default:  assert(!"error: not implemented or invalid dist_type");
    }
  }
  if(dist_type==0)  res=1-res;  // 1-intersection
  return res;
}

/* Compare color histograms of neighboring superpixels.
  dist_type = 1:'l1', 2:'l2', 0:'intersection', 12:'chi2'
*/
void compare_flow_hist( csr_matrix* ngh, const float_image* hist, const int dist_type ) {
  const int ns = hist->ty;
  assert(ngh->nr==ns && ngh->nc==ns);
  
  for(int i=0; i<ns; i++)
    for(int j=ngh->indptr[i]; j<ngh->indptr[i+1]; j++) {
      float dis = compare_hist(hist,i,ngh->indices[j],dist_type);
      assert(dis==dis);
      ngh->data[j] = dis;
    }
}


#include <unordered_set>
static inline long key( long i, long j ) {
  if(j>i) SWAP(i,j,long);   // always i<j 
  return i + (j<<32);
}
static inline int first(long i) {return int(i);}
static inline int second(long i) {return int(i >> 32);}
static int cmp_long ( const void* a, const void* b ) {
  long diff = (*(long*)a) - (*(long*)b);
  return (diff>0) - (diff<0);
}

/* connect temporal neighbours
*/
void _temporal_ngh_labels_to_spmat( const int n_labels, const int_layers* labels, coo_matrix* coo ) {
  const int npix = labels->tx * labels->ty;
  const int tz = labels->tz;
  const int* lab = labels->pixels;
  
  typedef unordered_set<long> ngh_t;
  ngh_t ngh;
  for(int j=0; j<tz-1; j++)
    for(int i=0; i<npix; i++) {
      int l0 = lab[i+j*npix];
      int l1 = lab[i+(j+1)*npix];
      if( l0!=l1 )
        ngh.emplace(key(l0,l1));
    }
  
  // convert result into a sparse graph
  coo->nr = n_labels;
  coo->nc = n_labels;
  assert(!coo->data);
  coo->row = NEWA(int, ngh.size());
  coo->col = NEWA(int, ngh.size());
  coo->data = NEWA(float, ngh.size());
  int n=0;
  for(auto it=ngh.begin(); it!=ngh.end(); ++it) {
    coo->row[n] = first(*it);
    coo->col[n] = second(*it);
    coo->data[n] = 1;
    n++;
  }
  coo->n_elem = n;
}



























