function [edges] = extract_image_edges( img, outname, ms, nms )

% example utilization:
% matlab -nojvm -nosplash -r "extract_image_edges('peppers.png','res.mat',1,0); exit"
%
% from scipy.io import loadmat
% m = loadmat('res.mat')
% from pylab import *; ion()
% imshow(m['edges'])

try
  cd /home/lear/revaud/scratch2/src/matlab/structured_edge_detection;
  addpath('piotr_toolbox/matlab/');
  addpath('piotr_toolbox/channels/');
  addpath('piotr_toolbox/images/');

  %% set opts for training (see edgesTrain.m)
  opts=edgesTrain();                % default options (good settings)
  opts.modelDir='models/';          % model will be in models/forest
  opts.modelFnm='modelFinal';       % model name
  opts.nPos=5e5; opts.nNeg=5e5;     % decrease to speedup training
  opts.useParfor=0;                 % parallelize if sufficient memory

  %% train edge detector (~30m/15Gb per tree, proportional to nPos/nNeg)
  model=edgesTrain(opts); % will load model if already trained

  %% set detection parameters (can set after training)
  model.opts.multiscale=ms;         % for top accuracy set multiscale=1
  model.opts.nTreesEval=4;          % for top speed set nTreesEval=1
  model.opts.nThreads=4;            % max number threads for evaluation
  model.opts.nms=nms;               % default=0, set to true to enable nms (fairly slow)

  %% detect edge and visualize results
  I = imread(img);
  if size(I,3) == 1, I = cat(3,I,I,I); end
  edges = edgesDetect(I,model);
  save(outname,'edges');

catch err
  disp(err.identifier)
end
