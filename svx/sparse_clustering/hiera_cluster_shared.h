#ifndef ___HIERA_CLUSTER_SHARED_H___
#define ___HIERA_CLUSTER_SHARED_H___
#include "numpy_image.h"


/* Remap an integer array in consecutive values
*/
int remap_consecutive( int_array* assign );


/* Compute the assignment for a given level in the tree
  nc: desired number of clusters
  unions: tree output by tree_cluster
  remap: assignment will be remapped in [0,nc-1]
  ucm_weights:  output final boundary weights
  return the actual number of clusters created
*/
int _tree_assign_n_cluster( int nc, const int_image* unions, int_array* _assign, int remap );



/* Compute the final edge weights given UCM segmentation tree
  ucm_weights:  output final boundary weights
*/
void compute_tree_ucm_weights( int_image* tree, csr_matrix* ucm_weights );
























#endif
