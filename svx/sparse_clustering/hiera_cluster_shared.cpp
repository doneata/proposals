#include "hiera_cluster_shared.h"
#include "std.h"
#include <vector>
#include <unordered_map>
#include "disjoint-set.h"
using namespace std;

#include "union_t.h"


/* Remap an integer array in consecutive values
*/
int remap_consecutive( int_array* _assign ) {
  // remap between assignment in [0,nc[
  int* assign = _assign->pixels;
  
  typedef unordered_map<int,int> remap_t;
  remap_t map;
  
  for(int i=0; i<_assign->tx; i++) {
    if(map.find(assign[i])==map.end()) {
      int nb = (signed)map.size();
      map[assign[i]] = nb;
    }
    
    assign[i] = map[assign[i]];
  }
  
  return (int)map.size();
}



/* Compute the assignment for a given level in the tree
  nc: desired number of clusters
  unions: tree output by tree_cluster
  remap: assignment will be remapped in [0,nc-1]
  return the actual number of clusters created
*/
int _tree_assign_n_cluster( const int nc, const int_image* unions, int_array* _assign, int remap ) {
  assert(unions->tx==3);
  const int nclass = _assign->tx;
  union_t* pairs = (union_t*)unions->pixels;
  union_t* end = pairs + unions->ty;
  
  int* assign = _assign->pixels;
  int* ranks = NEWAC(int,nclass);
  
  int i;
  for(i=0; i<nclass; i++) assign[i]=i;
  
  int nb=nclass;
  while(nb>nc && pairs!=end) {
    int i = pairs->a;
    int j = pairs->b;
    assert(0<=i && i<nclass);
    assert(0<=j && j<nclass);
    assert(!SameSet(assign,i,j));
    Union(assign,ranks,i,j);
    pairs++;
    nb--;
  }
  
  free(ranks);
  
  for(i=0; i<nclass; i++)
    FindRepresentative(assign,i); // settle representative
  
  if(remap) {
    int nc = remap_consecutive( _assign );
    assert(nb==nc);
  }
  
  return nb;
}


static int TreeRepresentative(const int* parents, int i) {
  while(parents[i]!=i)
    i = parents[i];
  return i;
}


static void PrintTreeRepresentative(int* parents, int* ranks, int i) {
  printf("%d (%d)",i,ranks[i]);
  while(parents[i]!=i) {
    i = parents[i];
    printf("--> %d (%d) ",i,ranks[i]);
  }
  printf("\n");
}

static int TreeUnion(int* parents, int* ranks, int i, int j) {
  int a = TreeRepresentative(parents,i);
  int b = TreeRepresentative(parents,j);
  assert(a!=b);
  
  if( ranks[a] < ranks[b] ) {
    parents[a] = b;
    return a;
    
  } else if( ranks[a] > ranks[b] ) {
    parents[b] = a;
    return b;
    
  } else {
    parents[a] = b;
    ranks[b]++;
    return a;
  }
}

static float TreeAncestor(const int* parents, const int* ranks, const float* weights, int i, int j) {
  float best = 0;
  while(i!=j) {
    if(ranks[i]<ranks[j]) {
      best = MAX(best,weights[i]);
      i = parents[i];
    } else {
      best = MAX(best,weights[j]);
      j = parents[j];
    }
  }
  return best;
}


/* Compute the final edge weights given UCM segmentation tree
  ucm_weights:  output final boundary weights
*/
void compute_tree_ucm_weights( int_image* unions, csr_matrix* ucm_weights ) {
  assert(unions->tx==3);
  union_t* pairs = (union_t*)unions->pixels;
  const int nclass = ucm_weights->nr;
  
  int* assign = NEWA(int, nclass);
  int* ranks = NEWAC(int, nclass);
  for(int i=0; i<nclass; i++) assign[i]=i;
  float* weights = NEWA(float, nclass);
  
  for(int n=0; n<unions->ty; n++) {
    int i = pairs[n].a;
    int j = pairs[n].b;
    weights[TreeUnion(assign,ranks,i,j)] = pairs[n].w;
  }
  
  // print result
  memset( ucm_weights->data, 0, ucm_weights->indptr[ucm_weights->nr]*sizeof(float));
  
  for(int r=0; r<ucm_weights->nr; r++) {
    int* cols = ucm_weights->indices + ucm_weights->indptr[r];
    float* data = ucm_weights->data + ucm_weights->indptr[r];
    
    int ncol = ucm_weights->indptr[r+1] - ucm_weights->indptr[r];
    for(int c=0; c<ncol; c++) 
      data[c] = TreeAncestor(assign,ranks,weights,r,cols[c]); 
  }
  
  free(assign);
  free(ranks);
  free(weights);
}


























