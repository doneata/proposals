#include "numpy_image.h"

/* Perform hierarchical clustering on a set of points, where only some
   pairwise distances are known. 
   
   dist_mat: must be a lower-triangular sparse square CSR matrix
   
   linkage: 's' (minimum), 'm' (maximum) or 'a' (average)
   
   weights: in case of average linkage, initial weights for each pair of elem
   
   res_out: succession of fusions
*/
void _temporal_tree_cluster_boost( const csr_matrix* dist_mat, int_array* _frames, 
                                   char linkage, int penalty_mode, float penalty_coef,
                                   csr_matrix* weights, int verbose, int_image* res_out2,
                                   int n_clusters_stop, coo_matrix* res_out );
