#include "nn_field.h"
#include "std.h"
#include <omp.h>
extern "C" {
#define integer int
#define real float
}
#include <unordered_set>
#include <unordered_map>
#include <algorithm>
using namespace std;

#include "graph_dist.h"


/* output an image of dense (x,y) coordinates (x in [0,tx[ and y in [0,ty[)
*/
void _xy_field( int_cube* res) {
  assert(res->tz==2);
  const int tx = res->tx;
  const int ty = res->ty;
  int* r = res->pixels;
  
  for(int j=0; j<ty; j++)
    for(int i=0; i<tx; i++) {
      *r++ = i;
      *r++ = j;
    }
}

#define InsertAfter(d,s,dist,ind,at) \
    {int _at=at; \
    while(d > dist[_at]) _at++; \
    memmove(dist+_at+1,dist+_at,(nn-1-_at)*sizeof(float));  \
    dist[_at] = d;  \
    memmove(ind+_at+1,ind+_at,(nn-1-_at)*sizeof(int));  \
    ind[_at] = s;}

#define Insert(d,s,dist,ind) InsertAfter(d,s,dist,ind,0)


/* Compute the closest seeds for each pixel given a euclidean distance.
*/
void _euclidean_nnfield( float_image* seeds, int nn, int_cube* best, float_cube* dis, int n_thread ) {
  ASSERT_SAME_SIZE(best,dis);
  assert(nn==dis->tz && nn==best->tz); // (float dist, int best) are stored consecutively
  const int tx = best->tx;
  const int ty = best->ty;
  assert(seeds->tx==2);
  const int ns = seeds->ty;
  
  // initialize with inf distances and -1 indices
  memset(dis->pixels,0x7F,tx*ty*nn*sizeof(float));
  memset(best->pixels,0xFF,tx*ty*nn*sizeof(int));
  const int r = sqrt(nn*tx*ty/ns);  // twice the expected radius to find all nn's
  
  for(int s=0; s<ns; s++) {
    float x = seeds->pixels[2*s];
    float y = seeds->pixels[2*s+1];
    
    // update every pixel
    const int imin = MAX(0,x-r);
    const int imax = MIN(tx,x+r+1);
    const int jmin = MAX(0,y-r);
    const int jmax = MIN(ty,y+r+1);
    for(int j=jmin; j<jmax; j++)
      for(int i=imin; i<imax; i++) {
        const float d = (i-x)*(i-x) + (j-y)*(j-y);
        
        float* dist = dis->pixels + (i+j*tx)*nn;
        if( d < dist[nn-1] ) { // better than last
          int* ind = best->pixels + (i+j*tx)*nn;
          Insert(d,s,dist,ind)
        }
      }
  }
  
  // second pass to correct when less than nn matches where found
  #ifdef USE_OPENMP
  #pragma omp parallel for num_threads(n_thread)
  #endif
  for(int j=0; j<ty; j++) {
    for(int i=0; i<tx; i++) {
      int* ind = best->pixels + (i+j*tx)*nn;
      if( ind[nn-1]<0 ) {
        float* dist = dis->pixels + (i+j*tx)*nn;
        memset(ind,0xFF,nn*sizeof(*ind));
        memset(dist,0x7F,nn*sizeof(*dist));
        for(int s=0; s<ns; s++) {
          float x = seeds->pixels[2*s];
          float y = seeds->pixels[2*s+1];
          const float d = (i-x)*(i-x) + (j-y)*(j-y);
          
          if( d < dist[nn-1] ) // better than last
            Insert(d,s,dist,ind)
        }
      }
    }
  }
}


template<typename Ti>
struct Tint_float_t {
  Ti i;
  float f;
  Tint_float_t(){}
  Tint_float_t(Ti i, float f):i(i),f(f){}
};
typedef Tint_float_t<int> int_float_t;
typedef Tint_float_t<long> long_float_t;

static int cmp_int_float ( const void* a, const void* b ) {
  return ((int_float_t*)a)->i - ((int_float_t*)b)->i;
}
static int cmp_long_float ( const void* a, const void* b ) {
  long diff = ((long_float_t*)a)->i - ((long_float_t*)b)->i;
  return (diff>0) - (diff<0);
}

static int cmp_int_float_float ( const void* a, const void* b ) {
  float diff = ((int_float_t*)a)->f - ((int_float_t*)b)->f;
  return (diff>0) - (diff<0);
}

template<typename T1>
static int compare(const void* a, const void* b) {
  T1 diff = *(T1*)a - *(T1*)b;
  return (diff>0) - (diff<0);
}
template<typename T1, typename T2>
static int arg_cmp(const void* a, const void* b, void* arr) {
  T2 diff = ((T2*)arr)[*(T1*)a] - ((T2*)arr)[*(T1*)b];
  return (diff>0) - (diff<0);
}

struct tuple_t {
  int* ref;
  int nn;
  
  tuple_t(int* ref, int nn):ref(ref),nn(nn){}
  
  void sort_median( float* vecs, float* tmpvec, int* order, int* scores ) {
    int i;
    for(i=0; i<nn; i++)
      tmpvec[i] = vecs[2*ref[i]];
    
    // arg sort the elements
    qsort_r(order, nn, sizeof(int), arg_cmp<int,float>, tmpvec);
    
    // add pyramidal score
    const float median = nn%2 ? tmpvec[order[nn/2]] : 
             (tmpvec[order[nn/2]] + tmpvec[order[nn/2-1]])/2;
    for(i=0; i<nn; i++)
      scores[i] += (tmpvec[i]-median)*(tmpvec[i]-median);
  }
};
struct hash_tuple {
  size_t operator () (const tuple_t& tup) const {
    const size_t b = 378551;
    const size_t a = 63689;
    size_t res = 0;
    for(int i=0; i<tup.nn; i++)
      res = (res*a + tup.ref[i])*b;
    return res;
  }
};
struct eq_tuple {
  bool operator () (const tuple_t& tup1,const tuple_t& tup2) const {return tup1.nn==tup2.nn;}
};

/* Select the best subset based on a median filter
*/
void _select_median_nn( int_image* best, int nr, float_image* vecs, float_image* dis, int n_thread ) {
  ASSERT_SAME_SIZE(best,dis);
  const int npix = best->ty;
  const int nn = best->tx;
  assert(0<nr && nr<nn);
  assert(vecs->tx==2);
  
  // begin by reordering best and dis
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif 
  {
    int_float_t* tmp = NEWA(int_float_t,nn);
    
    #ifdef USE_OPENMP
    #pragma omp for
    #endif 
    for(int p=0; p<npix; p++) {
      int* ind = best->pixels + p*nn;
      float* dist = dis->pixels + p*nn;
      for(int i=0; i<nn; i++) {
        tmp[i].i = ind[i];
        tmp[i].f = dist[i];
      }
      
      // sort along with distances
      qsort( tmp, nn, sizeof(*tmp), cmp_int_float );
      
      // write back
      for(int i=0; i<nn; i++) {
        ind[i] = tmp[i].i;
        dist[i] = tmp[i].f;
      }
    }
    
    free(tmp);
  }
  
  // now compute the median samples
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif 
  {
    typedef unordered_set<tuple_t,hash_tuple,eq_tuple> cache_t;
    cache_t done;
    const cache_t::iterator none = done.end();
    
    float* vec = NEWA(float, nn);
    int* order = NEWA(int, nn);
    int* score = NEWA(int,nn);
    
    #ifdef USE_OPENMP
    #pragma omp for
    #endif 
    for(int p=0; p<npix; p++) {
      int* ind = best->pixels + p*nn;
      float* dist = dis->pixels + p*nn;
      tuple_t pix( ind, nn );
      
      // check if tuple was already encountered
      cache_t::iterator f = done.find(pix);
      
      if(f==none) {
        done.insert(pix); // remember this pixel
        
        int i;
        // compute median
        memset(score,0,nn*sizeof(*score));
        for(i=0; i<nn; i++) order[i]=i;
        pix.sort_median( vecs->pixels  , vec, order, score ); // x
        pix.sort_median( vecs->pixels+1, vec, order, score ); // y
        
        // select the higest nr scores
        for(i=0; i<nn; i++) order[i]=i;
        qsort_r( order, nn, sizeof(int), arg_cmp<int,int>, score );  // sort best scores
        qsort( order, nr, sizeof(int), compare<int> );  // resort in asc index
        
        // write result
        for(int a=0; a<nr; a++) {
          int o = order[a];
          assert(a<=o);
          ind[a] = ind[o];
          dist[a] = dist[o];
        }
        
      } else {
        const int* ref = f->ref;
        
        // rearrange dists
        for(int a=0,b=0; a<nr; a++) {
          while(ind[b]<ref[a]) b++;
          assert(ind[b]==ref[a]);
          ind[a] = ind[b];
          dist[a] = dist[b];
        }
      }
    }
    
    free(vec);
    free(order);
    free(score);
  }
}




/* Compute the closest seeds for each pixel given a distance transform cost function.
*/
void _dist_trf_nnfield( int_image* seeds, int nn, float dmax, float_image* cost, dt_params_t* dt_params,
                        int_cube* best, float_cube* dis, int n_thread ) {
  ASSERT_SAME_SIZE(cost,best);
  ASSERT_SAME_SIZE(cost,dis);
  assert(nn==dis->tz && nn==best->tz); // (float dist, int best) are stored consecutively
  const int tx = cost->tx;
  const int ty = cost->ty;
  assert(seeds->tx==2);
  const int ns = seeds->ty;
  
  // initialize with inf distances and -1 indices
  memset(dis->pixels,0x7F,tx*ty*nn*sizeof(float));
  memset(best->pixels,0xFF,tx*ty*nn*sizeof(int));
  
  #ifdef USE_OPENMP
  omp_set_nested(0);  // no parallellization inside the loop
  #pragma omp parallel num_threads(n_thread)
  #endif 
  {
    float* tmp = NEWA(float,tx*ty);
    float_image dmap = {tmp,tx,ty};
    
    #ifdef USE_OPENMP
    #pragma omp for
    #endif 
    for(int s=0; s<ns; s++) {
      int seed[2];
      seed[0] = MIN(tx-1,seeds->pixels[2*s]);
      seed[1] = MIN(ty-1,seeds->pixels[2*s+1]);
      
      // compute distance map from this seed
      int bbox[4];
      if(dmax>0) {
        _weighted_distance_transform_thr( cost, dt_params, seed, dmax, &dmap, bbox );
        bbox[0]++; bbox[1]++;
      }else {
        memset(dmap.pixels,0x7F,tx*ty*sizeof(float));
        dmap.pixels[seed[0] + seed[1]*tx] = 0; // seed point
        _weighted_distance_transform( cost, dt_params, &dmap, NULL );
        bbox[0] = bbox[1] = 0;  bbox[2] = tx; bbox[3] = ty;
      }
      
      // update every pixel
      #ifdef USE_OPENMP
      #pragma omp critical
      #endif 
      for(int j=bbox[1]; j<bbox[3]; j++)
        for(int i=bbox[0]; i<bbox[2]; i++) {
        const float d = dmap.pixels[i+j*tx];
        
        float* dist = dis->pixels + (i+j*tx)*nn;
        if( d < dist[nn-1] )  { // better than last
          int* ind = best->pixels + (i+j*tx)*nn;
          Insert(d,s,dist,ind);
        }
      }
    }
    
    free(tmp);
  }
}


static int* prepare_seeds( int_image* _seeds, int tx, int ty ) {
  assert(_seeds->tx==2);
  const int ns = _seeds->ty;
  
  int* seeds = NEWA(int, 2*ns);
  memcpy(seeds,_seeds->pixels,2*ns*sizeof(*seeds));
  
  for(int s=0; s<ns; s++) {
    seeds[2*s+0] = MIN(tx-1,seeds[2*s+0]);
    seeds[2*s+1] = MIN(ty-1,seeds[2*s+1]);
  }
  
  return seeds;
}


static inline int bin_search(int* arr, int nb, int which) {
  int u=0,v=nb-1;
  if(which<arr[u] || arr[v]<which)  return -1;
  while(v-u>1) {
    int mid = (u+v)/2;
    if(arr[mid]<which)
      u = mid;
    else
      v = mid;
  }
  return (arr[u]==which) ? u : (arr[v]==which) ? v : -1;
}

static inline float line_distance( const int_Tuple2* a, const int_Tuple2* b, const float_image* cost ) {
  const int tx = cost->tx;
  const float* C = cost->pixels;
  
  int w = abs(a[0]-b[0]);
  int h = abs(a[1]-b[1]);
  
  float res = 0;
  if( w > h ) {
    if(a[0]>b[0]) SWAP(a,b,const int*);
    h = a[1]-b[1];
    
    for(int i=0; i<=w; i++)
      res += C[a[0]+i + (a[1]+(i*h)/w)*tx];
    
  } else {
    if(a[1]>b[1]) SWAP(a,b,const int*);
    w = a[0]-b[0];
    
    for(int i=0; i<=h; i++)
      res += C[a[0]+(i*w)/h + (a[1]+i)*tx];
  }
  
  // correct euclidean approximation
  w++; h++;
  return res * sqrt(w*w+h*h) / MAX(w,h);
}
static inline void rect_distance( const int_Tuple2* a, const int_Tuple2* b, const float_image* cost, 
                                  float* d1, float* d2 ) {
  const int tx = cost->tx;
  const float* C = cost->pixels;
  
  float r1=0, r2=0;
  
  if(a[0]>b[0]) SWAP(a,b,const int*);
  int w = b[0]-a[0];
  for(int i=0; i<=w; i++) {
    r1 += C[a[0]+i + a[1]*tx];
    r2 += C[a[0]+i + b[1]*tx];
  }
  r1 -= C[b[0] + a[1]*tx];  // don't count it twice
  r2 -= C[a[0] + b[1]*tx];
  
  int h = b[1]-a[1];
  if( h > 0 ) {
    h = abs(h);
    for(int i=0; i<=h; i++) {
      r2 += C[a[0] + (a[1]+i)*tx];
      r1 += C[b[0] + (a[1]+i)*tx];
    }
  } else {
    h = abs(h);
    for(int i=0; i<=h; i++) {
      r2 += C[a[0] + (b[1]+i)*tx];
      r1 += C[b[0] + (b[1]+i)*tx];
    }
  }
  
  *d1 = r1;
  *d2 = r2;
}


/* Compute fast approximate distance between pairs of seeds, given a cost map.
   The function modifies <ngh> data term.
*/
void compute_pairwise_dist_trf( int_image* _seeds, csr_matrix* ngh, float_image* cost, int n_thread ) {
  const int tx = cost->tx;
  const int ty = cost->ty;
  assert(_seeds->tx==2);
  const int ns = _seeds->ty;
  assert(ngh->nr==ns && ngh->nc==ns); 
  //assert(ngh->indptr[ns]%2==0 || !"error: ngh matrix must be symmetric and without diagonal");
  const int* indptr = ngh->indptr;
  
  // copy seeds in order to modify them
  const int* seeds = prepare_seeds(_seeds,tx,ty);
  
  // init distances to INF
  memset(ngh->data,0x7F,indptr[ns]*sizeof(float));
  
  #ifdef USE_OPENMP
  #pragma omp parallel for num_threads(n_thread)
  #endif 
  for(int s=0; s<ns; s++) {
    int* cols = ngh->indices + indptr[s];
    float* data = ngh->data + indptr[s];
    int ncols = indptr[s+1] - indptr[s];
    
    for(int i=0; i<ncols && cols[i]<s; i++) {  // lower triangle
      int c = cols[i];  // column = other seed
      float d1 = line_distance(seeds+2*s, seeds+2*c, cost);
      float d2,d3;
      rect_distance(seeds+2*s, seeds+2*c, cost, &d2, &d3);
      d2 = MIN(d2,d3);
      d1 = MIN(d1,d2);
      data[i] = d1;
      int j = indptr[c];
      int f = bin_search(ngh->indices+j,indptr[c+1]-j,s);
      if( f>=0 )  // we found a symmetric value
        ngh->data[j+f] = d1;
    }
  }
  
  free((void*)seeds);
}


/* convert nnf and dis to a sparse matrix.
  realloc: realloc if 1, else just copy pointers
  ns: number of seeds (~= max(nnf)+1)
*/
void nnf_to_spmat( int_image* nnf, float_image* dist, csr_matrix* csr, int realloc, int n_thread ) {
  ASSERT_SAME_SIZE(nnf,dist);
  const int npix = nnf->ty; // # pixels
  const int nn = nnf->tx; // # nn
  
  csr->nr = npix;
  csr->nc = npix;
  assert(!csr->indptr);
  csr->indptr = NEWA(int, npix+1);
  for(int n=0; n<=npix; n++)
    csr->indptr[n] = n*nn;
  
  if( realloc ) {
    csr->indices = NEWA(int, npix*nn);
    memcpy(csr->indices,nnf->pixels,npix*nn*sizeof(int));
    csr->data = NEWA(float, npix*nn);
    memcpy(csr->data,dist->pixels,npix*nn*sizeof(float));
  } else {
    csr->indices = nnf->pixels;
    csr->data = dist->pixels;
  }
  
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif
  {
    int_float_t* tmp = NEWA(int_float_t,nn);
    
    #ifdef USE_OPENMP
    #pragma omp for
    #endif
    for(int n=0; n<npix; n++) {
      for(int i=0; i<nn; i++) {
        tmp[i].i = csr->indices[n*nn+i];
        tmp[i].f = csr->data[n*nn+i];
      }
      qsort(tmp, nn, sizeof(int_float_t), cmp_int_float);
      for(int i=0; i<nn; i++) {
        csr->indices[n*nn+i] = tmp[i].i;
        csr->data[n*nn+i] = tmp[i].f;
      }
    }
    
    free(tmp);
  }
}


/* compute a graph of neighboring seeds based on euclidean distance
   The distance between each seed is re-estimated using a cost map (and a fast approximation).
   nnf and dis are optional
*/
void _ngh_seeds_euclidean( int_image* _seeds, int nn, float_image* cost, 
                           int_image* _nnf, float_image* _dis, csr_matrix* ngh, int n_thread ) {
  assert(_seeds->tx==2);
  const int ns = _seeds->ty;
  const int* seeds = _seeds->pixels;
  
  int_image nnf = {_nnf ? _nnf->pixels : NEWA(int,nn*ns),nn,ns};
  float_image dis = {_dis ? _dis->pixels : NEWA(float,nn*ns),nn,ns};
  memset(nnf.pixels,0xFF,ns*nn*sizeof(*nnf.pixels));
  memset(dis.pixels,0x7F,ns*nn*sizeof(*dis.pixels));
  
  #ifdef USE_OPENMP
  #pragma omp parallel for num_threads(n_thread)
  #endif
  for(int n=0; n<ns; n++) {
    const int i = seeds[2*n];
    const int j = seeds[2*n+1];
    int* ind = nnf.pixels + n*nn;
    float* dist = dis.pixels + n*nn;
    
    for(int s=0; s<ns; s++) if(s!=n) {
      float x = seeds[2*s];
      float y = seeds[2*s+1];
      const float d = (i-x)*(i-x) + (j-y)*(j-y);
      
      if( d < dist[nn-1] ) // better than last
        Insert(d,s,dist,ind)
    }
  }
  
  nnf_to_spmat(&nnf,&dis,ngh,(_nnf && _dis),n_thread);
}

static inline long key( long i, long j ) {
  if(j>i) SWAP(i,j,long);   // always i<j 
  return i + (j<<32);
}
static inline int first(long i) {return int(i);}
static inline int second(long i) {return int(i >> 32);}


struct border_t {
  float accu;
  int nb;
  
  border_t():accu(0),nb(0){}
  
  void add( float v, char linkage ) {
    switch(linkage) {
    case 's':
      if(!nb || accu>v) accu=v;
      break;  
    case 'm':
      if(accu<v) accu=v;
      break;  
    case 'a':
      accu += v;
      break;
    default:
      assert(!"error: unknown linkage");
    }
    nb++;
  }
  
  float get_val(char linkage) const {
    switch(linkage) {
    case 'a': return accu/nb;
    default:  return accu;
    }
  }
};


/* Find neighboring labels and store their relation in a sparse matrix.
   linkage = 's' (single == mininum) 'a' (average) or 'm' (maximum)
   nb_min: neighbors with less than nb_min border pixel are pruned.
*/
void _ngh_labels_to_spmat( int ns, int_image* labels, float_image* dmap, char linkage, int min_border_size, csr_matrix* csr ) {
  ASSERT_SAME_SIZE(labels, dmap);
  const int tx = labels->tx;
  const int ty = labels->ty;
  const int* lab = labels->pixels;
  const float* dis = dmap->pixels;
  
  typedef unordered_map<long,border_t> ngh_t;
  unordered_map<int,int> svx_size;
  unordered_map<int,int> svx_perimeter_size;
  for(int i=0; i<tx; i++)
    svx_size[lab[i]]++;
  ngh_t ngh;
  for(int j=1; j<ty; j++) {
    svx_size[lab[j*tx]]++;
    for(int i=1; i<tx; i++) {
      int l0 = lab[i+j*tx];
      svx_size[l0]++;
      int l1 = lab[i-1+j*tx];
      int l2 = lab[i+(j-1)*tx];
      if( l0!=l1 ) {
        long k = key(l0,l1);
        float d = dis[i+j*tx] + dis[i-1+j*tx];
        ngh[k].add(d,linkage);
        svx_perimeter_size[l0]++;
        svx_perimeter_size[l1]++;
      }
      if( l0!=l2 ) {
        long k = key(l0,l2);
        float d = dis[i+j*tx] + dis[i+(j-1)*tx];
        ngh[k].add(d,linkage);
        svx_perimeter_size[l0]++;
        svx_perimeter_size[l2]++;
      }
    }
  }
  
  // convert result into a sparse graph
  vector<long_float_t> sorted;
  for(ngh_t::iterator it=ngh.begin(); it!=ngh.end(); ++it) {
    int l0 = it->first>>32;
    int l1 = it->first<<32;
    if( it->second.nb <= min_border_size/100. * max(svx_perimeter_size[l0], svx_perimeter_size[l1]))
      continue;
    const float cost = it->second.get_val(linkage);
    sorted.emplace_back(it->first,cost);
    long symkey = (it->first>>32) + (it->first<<32);
    sorted.emplace_back(symkey,cost);  // add symmetric
  }
  // sort by (row,col) = (second,first)
  qsort(sorted.data(), sorted.size(), sizeof(long_float_t), cmp_long_float);
  
  csr->nr = ns;
  csr->nc = ns;
  assert(!csr->indptr);
  csr->indptr = NEWA(int, ns+1);
  csr->indices = NEWA(int, sorted.size());
  csr->data = NEWA(float, sorted.size());
  int n=0,r=0;
  csr->indptr[0] = 0;
  while( n<(signed)sorted.size() ) {
    int row = second(sorted[n].i);
    //assert(r<=row);
    while(r<row)  csr->indptr[++r] = n;
    csr->indices[n] = first(sorted[n].i);
    csr->data[n] = sorted[n].f;
    n++;
  }
  assert(r<ns);
  // finish row marker
  while(r<ns)
    csr->indptr[++r] = n;
}


/* visualize distances between neighboring nodes
*/
void _viz_edge_weights( const csr_matrix* edges, int_image* labels, float_image* _img ) {
  ASSERT_SAME_SIZE(labels,_img);
  const int tx = _img->tx;
  const int ty = _img->ty;
  
  typedef unordered_map<long,float> edges_t;
  edges_t ngh;
  
  // transfer csr to map
  for(int i=0; i<edges->nr; i++) {
    for(int j=edges->indptr[i]; j<edges->indptr[i+1]; j++)
      ngh.emplace(key(i,edges->indices[j]),edges->data[j]);
  }
  
  // transfer to image
  float* img = _img->pixels;
  const int* lab = labels->pixels;
  memset(img,0,tx*ty*sizeof(float));
  for(int j=1; j<ty; j++)
    for(int i=1; i<tx; i++) {
      int l0 = lab[i+j*tx];
      int l1 = lab[i-1+j*tx];
      int l2 = lab[i+(j-1)*tx];
      if( l0!=l1 ) {
        long k = key(l0,l1);
        img[i-1+j*tx] = img[i+j*tx] = ngh[k];
      }
      if( l0!=l2 ) {
        long k = key(l0,l2);
        img[i+j*tx] = img[i+(j-1)*tx] = ngh[k];
      }
    }
}


/* Compute the neighboring matrix between seeds as well as 
   the closest seed for each pixel.
   
   bdry_mode: how the distances between seeds are computed:
      =0 => estimated from boundaries in label map (more precise)
      >0 => estimated using line cost between matches center (faster).
            allocate <bdry_mode> euclidean neighbor per match, default = 6
*/
void _prepare_dist_trf_nnfield( int_image* _seeds, int bdry_mode, float_image* cost, dt_params_t* dt_params,
                                char linkage, int min_border_size,
                                int_image* labels, float_image* dmap, csr_matrix* ngh, int n_thread ) {
  const int tx = cost->tx;
  const int ty = cost->ty;
  assert(_seeds->tx==2);
  const int ns = _seeds->ty;
  
  // correct point outside image
  int* seeds = prepare_seeds(_seeds,tx,ty);
  
  // compute distance transform for all seeds altogether
  if(labels->pixels)
    assert(labels->tx==tx && labels->ty==ty);
  else
    *labels = {NEWA(int,tx*ty),tx,ty};
  if(dmap->pixels)
    assert(dmap->tx==tx && dmap->ty==ty);
  else
    *dmap = {NEWA(float,tx*ty),tx,ty};
  memset(dmap->pixels,0x7F,tx*ty*sizeof(float));
  for(int i=0; i<ns; i++) {
    int p = seeds[2*i] + seeds[2*i+1]*tx;
    labels->pixels[p] = i;
    dmap->pixels[p] = cost->pixels[p];
  }
  _weighted_distance_transform( cost, dt_params, dmap, labels );
  
  
  // compute distances between neighboring seeds
  if( bdry_mode == 0 )
    _ngh_labels_to_spmat( ns, labels, dmap, linkage, min_border_size, ngh );
  else {
    // find nearest neighbor of each matches
    _ngh_seeds_euclidean( _seeds, bdry_mode, cost, NULL, NULL, ngh, n_thread );
    // estimate distance between them
    compute_pairwise_dist_trf( _seeds, ngh, cost, n_thread );
  }
  
  free(seeds);
}


/* Compute the closest <nn> seeds for each seed given a distance transform cost function.
*/
void _dist_trf_seeds_fast( int_image* seeds, int bdry_mode,
                           float_image* cost, char linkage, int min_border_size, dt_params_t* dt_params,
                           int_image* labels, float_image* dmap,
                           int_image* nnf, float_image* dis, int n_thread ) {
  assert(seeds->tx==2);
  const int ns = seeds->ty;
  ASSERT_SAME_SIZE(nnf,dis);
  assert(nnf->ty==ns);
  const int nn = nnf->tx;
  
  // compute distance map and pairwise neighbor distances
  csr_matrix ngh = {0};
  _prepare_dist_trf_nnfield( seeds, bdry_mode, cost, dt_params, linkage, min_border_size, labels, dmap, &ngh, n_thread );
  
  // precompute nnf for each seed
  #ifdef USE_OPENMP
  #pragma omp parallel for num_threads(n_thread)
  #endif
  for(int n=0; n<ns; n++)
    _find_nn_graph_arr( &ngh, n, nn, nnf->pixels+n*nn, dis->pixels+n*nn );
  free(ngh.indptr);
  free(ngh.indices);
  free(ngh.data);
}



/* Compute the closest <nn> seeds for a list of given pixels  depending on 
   a distance transform cost function.
*/
void _dist_trf_nnfield_subset( int_image* seeds, int bdry_mode,
                               float_image* cost, char linkage, int min_border_size, dt_params_t* dt_params,
                               int_image* pixels, int_image* best, float_image* dist, int n_thread ) {
  ASSERT_SAME_SIZE(best,dist);
  const int tx = cost->tx;
  const int npix = pixels->ty;
  assert(best->ty==npix && dist->ty==npix);
  const int ns = seeds->ty;
  const int nn = best->tx;
  assert(dist->tx==nn);
  
  int_image labels = {0};
  float_image dmap = {0};
  int_image  nnf = {NEWA(int,ns*nn),nn,ns};
  float_image dis = {NEWA(float,ns*nn),nn,ns};
  // compute seeds proximity graph 
  _dist_trf_seeds_fast( seeds, bdry_mode, cost, linkage, min_border_size, dt_params, 
                        &labels, &dmap, &nnf, &dis, n_thread );
  
  // for each pixel, just look at closest seed and copy
  #ifdef USE_OPENMP
  #pragma omp parallel for num_threads(n_thread)
  #endif
  for(int _i=0; _i<npix; _i++) {
    const int i = pixels->pixels[2*_i] + pixels->pixels[2*_i+1]*tx;
    // this pixel is at distance <d> from seed <s>
    int s = labels.pixels[i];
    float d = dmap.pixels[i];
    memcpy(best->pixels+_i*nn,nnf.pixels+s*nn,nn*sizeof(nn));
    for(int j=0; j<nn; j++)
      dist->pixels[_i*nn+j] = d+dis.pixels[s*nn+j];  // distance plus constant
  }
  
  free(nnf.pixels);
  free(dis.pixels);
  free(labels.pixels);
  free(dmap.pixels);
}



/* Compute the closest <nn> seeds for each pixel given a distance transform cost function.
   
   bdry_mode: how the distances between seeds are computed:
      =0 => estimated from boundaries in label map (more precise)
      >0 => estimated using line cost between matches center (faster).
            allocate <bdry_mode> euclidean neighbor per match, default = 6
*/
void _dist_trf_nnfield_fast( int_image* seeds, int bdry_mode,
                             float_image* cost, char linkage, int min_border_size, dt_params_t* dt_params,
                             int_cube* best, float_cube* dist, int n_thread ) {
  ASSERT_SAME_CUBE_SIZE(best,dist);
  const int npix = cost->tx*cost->ty;
  const int ns = seeds->ty;
  const int nn = best->tz;
  
  int_image labels = {0};
  float_image dmap = {0};
  int_image  nnf = {NEWA(int,ns*nn),nn,ns};
  float_image dis = {NEWA(float,ns*nn),nn,ns};
  // compute seeds proximity graph 
  _dist_trf_seeds_fast( seeds, bdry_mode, cost, linkage, min_border_size, dt_params, 
                        &labels, &dmap, &nnf, &dis, n_thread );
  
  // for each pixel, just look at closest seed and copy
  #ifdef USE_OPENMP
  #pragma omp parallel for num_threads(n_thread)
  #endif
  for(int i=0; i<npix; i++) {
    // this pixel is at distance <d> from seed <s>
    int s = labels.pixels[i];
    float d = dmap.pixels[i];
    memcpy(best->pixels+i*nn,nnf.pixels+s*nn,nn*sizeof(nn));
    for(int j=0; j<nn; j++)
      dist->pixels[i*nn+j] = d+dis.pixels[s*nn+j];  // distance plus constant
  }
  
  free(nnf.pixels);
  free(dis.pixels);
  free(labels.pixels);
  free(dmap.pixels);
}


static int* prepare_permut( int ns ) {
  int* permut = NEWA(int, ns);
  {for(int i=0; i<ns; i++) {
    permut[i] = i;
    int r = rand() % (i+1);
    SWAP( permut[i], permut[r], int );
  }}
  return permut;
}

struct pix_score_t {
  int p,seed;
  float dis;
  pix_score_t(int p, int seed, float dis):
    p(p),seed(seed),dis(dis){}
};

static int cmp_pix ( const void* a, const void* b ) {
  const pix_score_t* pa = (pix_score_t*)a;
  const pix_score_t* pb = (pix_score_t*)b;
  int diff = pa->p - pb->p;
  if(!diff) diff = pa->seed - pb->seed;
  return (diff>0) - (diff<0);
}


/* Compute the closest seeds for each pixel given a distance transform cost function.
   The function returns at least <nmin> neighbors per pixel.
*/
void _dist_trf_dmaxfield_fast( int_image* seeds, int bdry_mode, float dmax, int nmin, int nmax, 
                               float_image* cost, char linkage, int min_border_size,
                               dt_params_t* dt_params, csr_matrix* best, int n_thread ) {
  const int npix = cost->tx*cost->ty;
  const int ns = seeds->ty;
  
  int_image labels = {0};
  float_image dmap = {0};
  csr_matrix ngh = {0};
  _prepare_dist_trf_nnfield( seeds, bdry_mode, cost, dt_params, linkage, min_border_size, 
                             &labels, &dmap, &ngh, n_thread );
  
  
  // precompute neighbors for each seed
  vector<list_node_dist_t> lists(ns);
  vector<list_node_dist_t> sorted_lists(ns);
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif
  {
    int* ind = NEWA(int,nmin);
    float* dis = NEWA(float,nmin);
    #ifdef USE_OPENMP
    #pragma omp for
    #endif
    for(int n=0; n<ns; n++) {
      _find_dmax_graph_arr( &ngh, n, dmax, lists[n] );
      
      if( (signed)lists[n].size() < nmin ) { // if not enough neighbors
        lists[n].clear();
        _find_nn_graph_arr( &ngh, n, nmin, ind, dis );
        lists[n].reserve(nmin);
        for(int j=0; j<nmin; j++)
          lists[n].emplace_back(ind[j],dis[j]);
        
      } else if( (signed)lists[n].size() > nmax ) {
        lists[n].erase(lists[n].begin()+nmax,lists[n].end());
      }
      
      // reorder lists in ascending indices
      sorted_lists[n] = (const list_node_dist_t&)lists[n];
      qsort(sorted_lists[n].data(), lists[n].size(), sizeof(node_dist_t), cmp_int_float);
      
      // just in case, sort the <nmin> first values
      qsort(lists[n].data(), nmin, sizeof(node_dist_t), cmp_int_float); 
    }
    free(ind);
    free(dis);
  }
  free(ngh.indptr);
  free(ngh.indices);
  free(ngh.data);
  
  
  // for each pixel, just look at closest seed and copy
  vector<int*> all_indptr(n_thread);
  vector<int*> all_indices(n_thread);
  vector<float*> all_data(n_thread);
  vector<int> all_size(n_thread);
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif
  {
    const int num_th = omp_get_thread_num();
    const int start = num_th*npix/n_thread;
    const int end = (num_th+1)*npix/n_thread;
    
    // first, estimate the res size for this thread
    int size = 0;
    for(int i=start; i<end; i++) 
      size += lists[labels.pixels[i]].size();
    
    // allocate
    int* indptr = all_indptr[num_th] = NEWA(int,end+1-start);
    int* ind = all_indices[num_th] = NEWA(int,size);
    float* data = all_data[num_th] = NEWA(float,size);
    
    // re-compute more precisely
    // this pixel is at distance <d> from seed <s>
    *indptr++ = 0;
    for(int i=start; i<end; i++) {
      const int label = labels.pixels[i];
      const list_node_dist_t& lis = sorted_lists[label];
      float d = dmap.pixels[i];
      
      int n=0;
      for(unsigned int j=0; j<lis.size(); j++) {
        float newd = d + lis[j].dis;
        if( newd<dmax ) {
          *data++ = newd;
          *ind++ = lis[j].node;
          n++;
        }
      }
      if( n < nmin ) {
        // not enough point, so we take the nearest neighbour 
        // in an unsorted list and resort the result after that
        data -= n;  // cancel
        ind -= n; // cancel
        const list_node_dist_t& lis = lists[label];
        n = 0;
        while(n<nmin) {
          *data++ = d + lis[n].dis;
          *ind++ = lis[n].node;
          n++;
        }
      }
      indptr[0] = indptr[-1] + n;
      indptr++;
    }
    all_size[num_th] = (ind - all_indices[num_th]);
  }
  free(labels.pixels);
  free(dmap.pixels);
  
  
  // merge everything into a csr matrix
  best->nr = npix;
  best->nc = ns;
  assert(!best->indptr);
  if(n_thread==1) {
    best->indptr = all_indptr[0];
    best->indices = all_indices[0];
    best->data = all_data[0];
  } else {
    unsigned int nb = 0;
    for(int i=0; i<n_thread; i++) 
      nb += all_size[i];
    
    int* indptr = best->indptr = NEWA(int, npix+1);
    best->indices = NEWA(int, nb);
    best->data = NEWA(float, nb);
    unsigned int n=0;
    for(int t=0; t<n_thread; t++) {
      const int nb = all_size[t];
      memcpy(best->indices+n,all_indices[t],nb*sizeof(int));
      memcpy(best->data+n,all_data[t],nb*sizeof(int));
      for(int i=0; all_indptr[t][i]<nb; i++)
        *indptr++ = n+all_indptr[t][i];
      n += nb;
      
      free(all_indptr[t]);
      free(all_indices[t]);
      free(all_data[t]);
    }
    *indptr = nb;
    assert(n==nb);
  }
}




/* Select the best subset based on a median filter for sparse matrix
   ratio: ratio of nn that are filtered (eg. 0.5 means half)
*/
void select_median_dmax( csr_matrix* best, float ratio, float_image* vecs, int n_thread ) {
  const int npix = best->nr;
  assert(0<ratio && ratio<1);
  assert(vecs->tx==2);
  
  int maxnn = 0;  // identifies the maximum number of nn
  for(int p=0; p<npix; p++) {
    int nb = best->indptr[p+1]-best->indptr[p];
    maxnn = MAX(maxnn, nb);
    assert(nb<=1 || best->indices[best->indptr[p]] < best->indices[best->indptr[p]+nb-1]);  // must be sorted
  }
  
  // compute the median samples
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif 
  {
    typedef unordered_set<tuple_t,hash_tuple,eq_tuple> cache_t;
    cache_t done;
    const cache_t::iterator none = done.end();
    
    float* vec = NEWA(float, maxnn);
    int* order = NEWA(int, maxnn);
    int* score = NEWA(int, maxnn);
    
    #ifdef USE_OPENMP
    #pragma omp for
    #endif 
    for(int p=0; p<npix; p++) {
      const int nn = best->indptr[p+1] - best->indptr[p];
      if(nn==0) continue;
      int* ind = best->indices + best->indptr[p];
      float* dist = best->data + best->indptr[p];
      tuple_t pix( ind, nn );
      
      const int nr = 1+int(nn*ratio);
      
      // check if tuple was already encountered
      cache_t::iterator f = done.find(pix);
      
      if(f==none) {
        done.insert(pix); // remember this pixel
        
        int i;
        // compute median
        memset(score,0,nn*sizeof(*score));
        for(i=0; i<nn; i++) order[i]=i;
        pix.sort_median( vecs->pixels  , vec, order, score ); // x
        pix.sort_median( vecs->pixels+1, vec, order, score ); // y
        
        // select the higest nr scores
        for(i=0; i<nn; i++) order[i]=i;
        qsort_r( order, nn, sizeof(int), arg_cmp<int,int>, score );  // sort best scores
        qsort( order, nr, sizeof(int), compare<int> );  // resort in asc index
        
        // write result
        int a;
        for(a=0; a<nr; a++) {
          int o = order[a];
          assert(a<=o);
          ind[a] = ind[o];
          dist[a] = dist[o];
        }
        for(; a<nn; a++) {
          ind[a] = -1;
          dist[a] = INFINITY;  // virtuall remove other points
        }
      } else {
        const int* ref = f->ref;
        
        // rearrange dists
        int a,b;
        for(a=0,b=0; a<nr; a++) {
          while(ind[b]<ref[a]) b++;
          assert(ind[b]==ref[a]);
          ind[a] = ind[b];
          dist[a] = dist[b];
        }
        for(; a<nn; a++) {
          ind[a] = -1;
          dist[a] = INFINITY;  // virtuall remove other points
        }
      }
    }
    
    free(vec);
    free(order);
    free(score);
  }
}




/* Kernel interpolation for sparse matrix
  Result for each dimension is x = \sum(w_i*x_i) / \sum(w_i)
*/
void _kernel_interpolate_csr( csr_matrix* nnf, float_image* vects, float_image* res, int n_thread ) {
  assert(vects->tx==2);
  const int nr = nnf->nr; // number of pixels
  assert(nnf->nc==vects->ty);
  assert(res->tx==2 && res->ty==nr);
  
  // init res to 0
  memset(res->pixels,0,nr*2*sizeof(float));
  
  int r;
  #ifdef USE_OPENMP
  #pragma omp parallel for num_threads(n_thread)
  #endif 
  for(r=0; r<nr; r++) {
    float x=0,y=0,sumw=1e-8;
    for(int c=nnf->indptr[r]; c<nnf->indptr[r+1]; c++) {
      float w = nnf->data[c];
      sumw += w;
      int seed = nnf->indices[c];
      x += w * vects->pixels[ 2*seed   ];
      y += w * vects->pixels[ 2*seed+1 ];
    }
    res->pixels[2*r  ] = x/sumw;
    res->pixels[2*r+1] = y/sumw;
  }
}




/* Extract a lot of regions given a distance transform cost function.
  dmax: max distance to form a region
  dmin: distance thr to avoid picking a seed in a previously found region
*/
void _random_regions_dist_trf( float dmin, float dmax, float_image* cost, dt_params_t* dt_params,
                               csr_matrix* best, int n_thread ) {
  const int tx = cost->tx;
  const int ty = cost->ty;
  
  // each pixel remember the best distance so far
  const int tx4 = tx/4, ty4 = ty/4;
  int* permut = prepare_permut(tx4*ty4);
  
  float* best_pix = NEWA(float, tx*ty);
  memset(best_pix,0x7F,tx*ty*sizeof(float));
  
  typedef vector<int> pix_list_t;
  vector<pix_list_t*> regions; // list of sparse regions
  
  #ifdef USE_OPENMP
  omp_set_nested(0);  // no parallellization inside the loop
  #pragma omp parallel num_threads(n_thread)
  #endif 
  {
    float* tmp = NEWA(float,tx*ty);
    float_image dmap = {tmp,tx,ty};
    
    #ifdef USE_OPENMP
    #pragma omp for
    #endif
    for(int _s=0; _s<tx4*ty4; _s++) {
      int s = permut[_s]; // chosen seed
      int seed[2] = {(s%tx4)*4+2, (s/tx4)*4+2};
      assert(seed[0]<tx && seed[1]<ty);
      s = seed[0] + seed[1]*tx;
      if(best_pix[s]<=dmin) continue;  // already 'done'
      
      // compute distance map from this seed
      int bbox[4];
      _weighted_distance_transform_thr( cost, dt_params, seed, dmax, &dmap, bbox );
      bbox[0]++; bbox[1]++;
      
      // update every pixel
      pix_list_t* res = new pix_list_t;
      for(int j=bbox[1]; j<bbox[3]; j++)
        for(int i=bbox[0]; i<bbox[2]; i++) {
          const int p = i+j*tx;
          const float d = dmap.pixels[p];
          
          if( d < dmax )  {
            res->emplace_back(p);
            if( d < best_pix[p] ) 
              best_pix[p] = d;
          }
        }
      #ifdef USE_OPENMP
      #pragma omp critical
      #endif
      {
        regions.push_back(res);  // create new region
        #pragma omp flush(regions)
      }
    }
    
    free(tmp);
  }
  
  free(permut);
  
  // combine everything into a sparse image
  unsigned int nb=0;
  for(unsigned int l=0; l<regions.size(); l++) 
    nb += regions[l]->size();
  
  best->nr = regions.size();
  best->nc = tx*ty;
  assert(!best->indptr);
  best->indptr = NEWA(int, regions.size()+1);
  best->indices = NEWA(int, nb);
  best->data = NEWA(float, nb);
  unsigned int n=0,r=0;
  for(; r<regions.size(); r++) {
    best->indptr[r] = n;
    const pix_list_t& reg = *regions[r];
    for(unsigned int i=0; i<reg.size(); i++,n++) {
      best->indices[n] = reg[i];
      best->data[n] = 1;
    }
    delete &reg;
  }
  best->indptr[r] = n;
  assert(n==nb);
  
  free(best_pix);
}


































/* Compute the closest seeds for each seed given a distance transform cost function.
   The function returns at least <nmin> neighbors per pixel.
*/
void _dist_trf_dmax_seeds_fast( int_image *labels, int_image* seeds, int bdry_mode, float dmax, int nmin, int nmax, 
                               float_image* cost, char linkage, int min_border_size,
                               dt_params_t* dt_params, csr_matrix* best, int n_thread ) {
  const int ns = seeds->ty;
  
  float_image dmap = {0};
  csr_matrix ngh = {0};
  _prepare_dist_trf_nnfield( seeds, bdry_mode, cost, dt_params, linkage, min_border_size, 
                             labels, &dmap, &ngh, n_thread );
  
  
  // precompute neighbors for each seed
  vector<list_node_dist_t> lists(ns);
  vector<list_node_dist_t> sorted_lists(ns);
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif
  {
    int* ind = NEWA(int,nmin);
    float* dis = NEWA(float,nmin);
    #ifdef USE_OPENMP
    #pragma omp for
    #endif
    for(int n=0; n<ns; n++) {
      _find_dmax_graph_arr( &ngh, n, dmax, lists[n] );
      if( (signed)lists[n].size() < nmin ) { // if not enough neighbors
        lists[n].clear();
        _find_nn_graph_arr( &ngh, n, nmin, ind, dis );
        lists[n].reserve(nmin);
        for(int j=0; j<nmin; j++)
	  {
	    lists[n].emplace_back(ind[j],dis[j]);
	  }
        
      } else if( (signed)lists[n].size() > nmax ) {
        lists[n].erase(lists[n].begin()+nmax,lists[n].end());
      }
      
      // reorder lists in ascending indices
      sorted_lists[n] = (const list_node_dist_t&)lists[n];
      qsort(sorted_lists[n].data(), lists[n].size(), sizeof(node_dist_t), cmp_int_float);
      
      // just in case, sort the <nmin> first values
      qsort(lists[n].data(), nmin, sizeof(node_dist_t), cmp_int_float); 
    }
    free(ind);
    free(dis);
  }
  free(ngh.indptr);
  free(ngh.indices);
  free(ngh.data);

  vector<int*> all_indptr(n_thread);
  vector<int*> all_indices(n_thread);
  vector<float*> all_data(n_thread);
  vector<int> all_size(n_thread);
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif
  {
    const int num_th = omp_get_thread_num();
    const int start = num_th*ns/n_thread;
    const int end = (num_th+1)*ns/n_thread;
    // estimate the res size for this thread
    int size = 0;
    for(int i=start ; i<end ; i++)
      size += lists[i].size();
    // allocate
    int *indptr = all_indptr[num_th] = NEWA(int, end+1-start);
    int *ind = all_indices[num_th] = NEWA(int,size);
    float *data = all_data[num_th] = NEWA(float, size);
    *indptr++ = 0;
    for(int i =start ; i<end ; i++)
      {
	const list_node_dist_t& lis = sorted_lists[i];
	const float d = 0.0f; //cost->pixels[ seeds->pixels[2*i+1] * cost->tx + seeds->pixels[2*i] ];
	int n = 0;
	for( uint j=0 ; j<lis.size() ; j++)
	  {
	     float newd = d +lis[j].dis;
	     if(newd < dmax)
	        {
	           *data++ = newd;
	           *ind++ = lis[j].node;	  
	           n++;
                }
          }
	if( n < nmin ) 
	  {
            // not enough point, so we take the nearest neighbour 
            // in an unsorted list and resort the result after that
            data -= n;  // cancel
	    ind -= n; // cancel
	    const list_node_dist_t& lis = lists[i];
	    n = 0;
	    while(n<nmin) 
	      {
	        *data++ = d+lis[n].dis;
		*ind++ = lis[n].node;
		n++;
              }
          }
	indptr[0] = indptr[-1] + n;
	indptr++;
      }
    all_size[num_th] = (ind - all_indices[num_th]);
  }

  free(dmap.pixels);

  // merge everything into a csr matrix
  best->nr = ns;
  best->nc = ns;
  assert(!best->indptr);
  if(n_thread==1) {
    best->indptr = all_indptr[0];
    best->indices = all_indices[0];
    best->data = all_data[0];
  } else {
    unsigned int nb = 0;
    for(int i=0; i<n_thread; i++) 
      nb += all_size[i];
    
    int* indptr = best->indptr = NEWA(int, ns+1);
    best->indices = NEWA(int, nb);
    best->data = NEWA(float, nb);
    unsigned int n=0;
    for(int t=0; t<n_thread; t++) {
      const int nb = all_size[t];
      memcpy(best->indices+n,all_indices[t],nb*sizeof(int));
      memcpy(best->data+n,all_data[t],nb*sizeof(int));
      for(int i=0; all_indptr[t][i]<nb; i++)
        *indptr++ = n+all_indptr[t][i];
      n += nb;
      
      free(all_indptr[t]);
      free(all_indices[t]);
      free(all_data[t]);
    }
    *indptr = nb;
    assert(n==nb);
  }
}


void _apply_homographies(const float_image* homographies, float_image* newvects, const int_image *labels, int n_thread)
{
  #ifdef USE_OPENMP
  #pragma omp parallel for num_threads(n_thread)
  #endif
  for( int j = 0 ; j<labels->ty ; j++)
    {
      for(int i=0 ; i<labels->tx ; i++)
	{
	  const int n = j*labels->tx+i;
	  const int s = labels->pixels[n];
	  float *m = &homographies->pixels[8*s];
	  const float D = m[6]*i + m[7]*j + 1.0;
	  newvects->pixels[2*n  ] = (m[0]*i + m[1]*j + m[2])/D -i;
	  newvects->pixels[2*n+1] = (m[3]*i + m[4]*j + m[5])/D -j;
	}
    }
}


void _fit_nadarayawatson(float_image *newvects, const csr_matrix *nnf, const int_image *seeds, const float_image *vects, int n_thread)
{
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif
  for(int i = 0 ; i<seeds->ty ; i++)
    {
      const int start = nnf->indptr[i], end = nnf->indptr[i+1];
      float u = 0.0f, v = 0.0f, s = 0.0f;
      for(int j = start ; j<end ; j++)
	{
	  const int jj = nnf->indices[j];
	  if(jj==-1)
	    continue;
	  u += nnf->data[j]*vects->pixels[2*jj];
	  v += nnf->data[j]*vects->pixels[2*jj+1];
	  s += nnf->data[j];
	}
      newvects->pixels[2*i  ] = u/s;
      newvects->pixels[2*i+1] = v/s;
    }
}

void _apply_nadarayawatson(const float_image *seedsvects, float_image *newvects, const int_image *labels, int n_thread)
{
  #ifdef USE_OPENMP
  #pragma omp parallel num_threads(n_thread)
  #endif
  for( int j = 0 ; j<labels->ty ; j++)
    {
      for(int i=0 ; i<labels->tx ; i++)
	{
	  const int n = j*labels->tx+i;
	  const int s = labels->pixels[n];
	  newvects->pixels[2*n  ] = seedsvects->pixels[2*s];
	  newvects->pixels[2*n+1] = seedsvects->pixels[2*s+1];
	}
    }
}
