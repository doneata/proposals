#include "weighted_dist_trf.h"
#include "std.h"
#include <vector>

#define IF_ASSERT(r) ; //assert(r)

static float sweep( const float_image* cost, float_image* res, const char x, const char y ) {
  int i, j;
  float t0, t1, t2, C, max_diff = 0.0;
  const int tx = res->tx, ty = res->ty;
  float* A = res->pixels;
  const float* Cost = cost->pixels;
  
  const int bx = x>0 ? 0 : tx-1;
  const int by = y>0 ? 0 : ty-1;
  const int ex = x>0 ? tx : -1;
  const int ey = y>0 ? ty : -1;
  
  for(j=by; j!=ey; j+=y)
    for(i=bx; i!=ex; i+=x) {
      t1 = (j==by) ? INFINITY : A[i + (j-y)*tx];
      t2 = (i==bx) ? INFINITY : A[i-x + j*tx];
      float dt12 = fabs(t1-t2);
      C = Cost[i + j*tx];
      
      if( dt12 > C ) 
        t0 = MIN(t1, t2) + C;  // handle degenerate case
      else
        t0 = 0.5*(t1 + t2 + sqrt(2*C*C - dt12*dt12));
      
      if( t0 < A[i + j*tx] )  {
        max_diff = MAX(max_diff, A[i + j*tx] - t0);
        A[i + j*tx] = t0;
      }
  }
  
  return max_diff;
}


static float arg_sweep( const float_image* cost, float_image* res, int_image* labels, const char x, const char y ) {
  int i, j;
  const int tx = res->tx, ty = res->ty;
  float* A = res->pixels;
  int* L = labels->pixels;
  const float* Cost = cost->pixels;
  
  const int bx = x>0 ? 0 : tx-1;
  const int by = y>0 ? 0 : ty-1;
  const int ex = x>0 ? tx : -1;
  const int ey = y>0 ? ty : -1;
  
  float t0, t1, t2, C, max_diff = 0.0;
  int l0, l1, l2;
  for(j=by; j!=ey; j+=y)
    for(i=bx; i!=ex; i+=x) {
      if(j==by) {
        t1 = INFINITY;
        l1 = -1;
      } else {
        t1 = A[i + (j-y)*tx];
        l1 = L[i + (j-y)*tx];
      }
      if(i==bx) {
        t2 = INFINITY;
        l2 = -1;
      } else {
        t2 = A[i-x + j*tx];
        l2 = L[i-x + j*tx];
      }
      float dt12 = fabs(t1-t2);
      C = Cost[i + j*tx];
      
      if( dt12 > C ) {  // handle degenerate case
        if( t1 < t2 ) {
          t0 = t1 + C;
          l0 = l1;
        } else {
          t0 = t2 + C;
          l0 = l2;
        }
//      } else if(l1<0 || l2<0 || l1==l2) {
//        t0 = 0.5*(t1 + t2 + sqrt(2*C*C - dt12*dt12));
//        l0 = (l1<0) ? l2 : l1;
//      } else
//        continue;
      } else {
        t0 = 0.5*(t1 + t2 + sqrt(2*C*C - dt12*dt12));
        l0 = (t1<t2) ? l1 : l2;
      }
      
      if( t0 < A[i + j*tx] )  {
        max_diff = MAX(max_diff, A[i + j*tx] - t0);
        A[i + j*tx] = t0;
        L[i + j*tx] = l0;
      }
  }
  
  return max_diff;
}

static float sweep_bbox( const float_image* cost, float_image* res,  const char x, const char y, const int* bbox ) {
  int i, j;
  float t0, t1, t2, C, max_diff = 0.0;
  const int tx = res->tx;
  float* A = res->pixels;
  const float* Cost = cost->pixels;
  
  const int bx = (x>0 ? bbox[0] : bbox[2]) + x;
  const int by = (y>0 ? bbox[1] : bbox[3]) + y;
  const int ex = x>0 ? 2 : 0;
  const int ey = y>0 ? 3 : 1;
  
  for(j=by; j!=bbox[ey]; j+=y)
    for(i=bx; i!=bbox[ex]; i+=x) {
      t1 = (j==by) ? INFINITY : A[i + (j-y)*tx];
      t2 = (i==bx) ? INFINITY : A[i-x + j*tx];
      float dt12 = fabs(t1-t2);
      C = Cost[i + j*tx];
      
      if( dt12 > C ) 
        t0 = MIN(t1, t2) + C;  // handle degenerate case
      else
        t0 = 0.5*(t1 + t2 + sqrt(2*C*C - dt12*dt12));
      
      if( t0 < A[i + j*tx] )  {
        max_diff = MAX(max_diff, A[i + j*tx] - t0);
        A[i + j*tx] = t0;
      }
  }
  
  return max_diff;
}


template<char x, char y>
static float sweep_thr( const float_image* cost, float_image* res,  
                        int* bbox, const float thr ) {
  int i, j;
  float t0, t1, t2, C, max_diff = 0.0;
  const int tx = res->tx, ty = res->ty;
  float* A = res->pixels;
  const float* Cost = cost->pixels;
  
  const int bx = (x>0 ? bbox[0] : bbox[2]) + x;
  const int by = (y>0 ? bbox[1] : bbox[3]) + y;
  const int ex = x>0 ? 2 : 0;
  const int ey = y>0 ? 3 : 1;
  
  for(j=by; j!=bbox[ey]; j+=y)
    for(i=bx; i!=bbox[ex]; i+=x) {
      t1 = (j==by) ? INFINITY : A[i + (j-y)*tx];
      t2 = (i==bx) ? INFINITY : A[i-x + j*tx];
      float dt12 = fabs(t1-t2);
      C = Cost[i + j*tx];
      
      if( dt12 > C ) 
        t0 = MIN(t1, t2) + C;  // handle degenerate case
      else
        t0 = 0.5*(t1 + t2 + sqrt(2*C*C - dt12*dt12));
      
      if( t0 < A[i + j*tx] )  {
        max_diff = MAX(max_diff, A[i + j*tx] - t0);
        A[i + j*tx] = t0;
      }
      
      // update bounding box
      if( A[i + j*tx]<thr && i>0 && j>0 && i<tx-1 && j<ty-1 ) {
        if(x<0){if(i-1==bbox[0]) bbox[0]--;}else{if(i+1==bbox[2]) bbox[2]++;}
        if(y<0){if(j-1==bbox[1]) bbox[1]--;}else{if(j+1==bbox[3]) bbox[3]++;}
      }
  }
  
  return max_diff;
}


void set_default_dt_params( dt_params_t* params ) {
  params->max_iter = 40;
  params->min_change = 1;
}

#define DEFAULT_dt_params(dt_params)  \
  dt_params_t tmp_dt_params;\
  if(!dt_params){set_default_dt_params(&tmp_dt_params);dt_params=&tmp_dt_params;}

/* Compute distance map from a given seeds (in res) and a cost map.
  if labels!=NULL:  labels are propagated along with distance map 
                    (for each pixel, we remember the closest seed)
*/
float _weighted_distance_transform( const float_image* cost, const dt_params_t* dt_params, 
                                   float_image* res, int_image* labels ) {
  assert( cost && res );
  ASSERT_SAME_SIZE(cost,res);
  if(labels)  ASSERT_SAME_SIZE(res,labels);
  DEFAULT_dt_params(dt_params)
  assert( dt_params->min_change >= 0 );
  
  const char x[4] = {-1,1,1,-1};
  const char y[4] = {1,1,-1,-1};
  int i = 0, end_iter = 4;
  float change = -1;
  while(++i <= end_iter) {
    change = labels ? arg_sweep(cost, res, labels, x[i%4], y[i%4]) :
                                sweep(cost, res,         x[i%4], y[i%4]);
    if( change > dt_params->min_change )
      end_iter = MIN(dt_params->max_iter, i+3); // finish the turn
  }
  return change;
}


/* Compute distance map from a given seeds (in res) and a cost map.
*/
float _weighted_distance_transform_bbox( const float_image* cost, const dt_params_t* dt_params, 
                                        float_image* res, const int* bbox ) {
  assert( cost && res );
  ASSERT_SAME_SIZE(cost,res);
  DEFAULT_dt_params(dt_params)
  assert( dt_params->min_change >= 0 );
  
  const char x[4] = {-1,1,1,-1};
  const char y[4] = {1,1,-1,-1};
  int i = 0, end_iter = 4;
  float change = -1;
  while(++i <= end_iter) {
    change = sweep_bbox(cost, res, x[i%4], y[i%4], bbox);
    if( change > dt_params->min_change )
      end_iter = MIN(dt_params->max_iter, i+3); // finish the turn
  }
  return change;
}


/* Compute distance map from a given seeds (in res) and a cost map.
   The distance map will only be partially computed, ie. in all areas where
   distance < thr.
*/
float _weighted_distance_transform_thr( const float_image* cost, const dt_params_t* dt_params, 
                                       const int_Tuple2* seed, const float thr, 
                                       float_image* res, int_Tuple4* bbox ) {
  // init res with INF
  memset(res->pixels,0x7F,res->tx*res->ty*sizeof(*res->pixels));
  res->pixels[seed[0] + seed[1]*res->tx] = 0; // set seed
  
  // init bbox
  bbox[0] = seed[0]-1;
  bbox[1] = seed[1]-1;
  bbox[2] = seed[0]+1;
  bbox[3] = seed[1]+1;
  
  return _weighted_distance_transform_thr_ex( cost, dt_params, thr, res, bbox );
}

float _weighted_distance_transform_thr_ex( const float_image* cost, const dt_params_t* dt_params, const float thr, 
                                          float_image* res, int_Tuple4* bbox ) {
  assert( cost && res );
  ASSERT_SAME_SIZE(cost,res);
  DEFAULT_dt_params(dt_params)
  assert( dt_params->min_change >= 0 );
  
  int i = 0, end_iter = 4;
  float change = -1;
  while(++i <= end_iter) {
    switch(i%4) {
    case 1: change = sweep_thr< 1, 1>(cost, res, bbox, thr); break;
    case 2: change = sweep_thr< 1,-1>(cost, res, bbox, thr); break;
    case 3: change = sweep_thr<-1,-1>(cost, res, bbox, thr); break;
    case 0: change = sweep_thr<-1, 1>(cost, res, bbox, thr); break;
    }
    if( change > dt_params->min_change )
      end_iter = MIN(dt_params->max_iter, i+3); // finish the turn
  }
  return change;
}


/* Find minimum path from a point to another one given a cost map. 
   (x, y) denotes the end point.
    
   The user must provide the distance_map, generally computed with:
      dmap = weighted_distance_transform( cost, params, res )
   and res[:]=0 excpet at res[start point] = 1
   
   Important: The cost must be >0 everywhere, otherwise the path will get stuck.
*/
void weighted_shortest_path( float_image* dmap, int x, int y, int_image* res_out ) {
  const int tx = dmap->tx;
  const int ty = dmap->ty;
  assert(0<=x && x<tx && 0<=y && y<ty);
  const float* dm = dmap->pixels;
  
  std::vector<int> res;
  float best = dm[y*tx+x], cur;
  do {
    res.emplace_back(x);
    res.emplace_back(y);
    cur = best;
    
    // search the minimum position around
    best = cur;
    int miny = MAX(0,y-1), maxy = MIN(y+2,ty);
    int minx = MAX(0,x-1), maxx = MIN(x+2,tx);
    #define trypos(i,j) {if( dm[j*tx+i]<best ) {best=dm[j*tx+i]; x=i; y=j;}}
    int u=x, v=y;
    trypos(u,miny)
    trypos(minx,v)
    trypos(maxx,v)
    trypos(u,maxy)
    #undef trypos
//    for(int j=miny; j<maxy; j++)
//      for(int i=minx; i<maxx; i++) {
//        if( dm[j*tx+i]<best ) {best=dm[j*tx+i]; x=i; y=j;}
//      }
    
  } while( best < cur );
  
  res_out->pixels = res.data();
  res_out->tx = 2;
  res_out->ty = (int)res.size()/2;
  memset(&res,0,sizeof(res)); // erase vector
}




/* How hexa-grid works in practice:

ty, tx = 40, 11
r = empty((ty,tx),dtype=int32)
step_half = 10
true_step = 2*step_half
gx = 2+(tx+step_half-1)/true_step
gy = 3+(ty+step_half-2)/step_half
print gy,gx
iis = set()
jjs = set()
for y in range(r.shape[0]):
  for x in range(r.shape[1]):
    u = (x+y)/true_step
    v = (x-y)/true_step
    j = (u-v)
    i = (u+v+(j%2))/2
    j += 1
    assert i>=0, pdb.set_trace()
    iis.add(i)
    jjs.add(j)
    r[y,x] = i + j*gx
print "all i's =",iis
print "all j's =",jjs
print r[::5,::5]
imshow(r,interpolation='nearest')
pdb.set_trace()
*/

/* return width of small image
*/
inline int get_gx( int hexagrid, int tx, int step ) {
  if( hexagrid ) 
    return 2+(tx+step/2-1)/step;
  else
    return (tx+2*step-2)/step;
}
inline int get_gy( int hexagrid, int ty, int step ) {
  if( hexagrid ) 
    return 3+(ty+step/2-2)*2/step;
  else
    return (ty+2*step-2)/step;
}

/* return the position of top-left nearest hub point
*/
inline void img_to_topleft_hub( const approx_dt_t* adt, int x, int y, int* u, int* v ) {
  int step = adt->step_global;
  if( adt->hexagrid ) {
    int u = (x+y)/step;
    int v = x-y;v=v/step-(v<0); // correct division
    y = (u-v);
    x = (u+v+(y%2))/2;
    y += 1;
  } else {
    x /= step;
    y /= step;
    if(x>=adt->global.tx-1) x--;
    if(y>=adt->global.ty-1) y--;
  }
  
  *u = x;
  *v = y;
//  return x + y*adt->global.tx;
}


approx_dt_t* _new_adt( int tx, int ty, int step_global, int step_local, int hexagrid ) {
  // size of global image
  assert( !hexagrid || (step_global%2==0) );
  const int gx = get_gx(hexagrid, tx, step_global);
  const int gy = get_gy(hexagrid, ty, step_global);
  const int np = gx*gy;
  
  // size of local image
  assert(step_global % step_local == 0||!"error: steps have to be multiple");
  const int ls = 2*step_global/step_local+1;
  
  approx_dt_t* adt = NEW(approx_dt_t);
  adt->tx = tx;
  adt->ty = ty;
  adt->step_local = step_local;
  adt->step_global = step_global;
  adt->hexagrid = hexagrid;
  adt->global = (float_layers){NEWA(float,gx*gy*np),gx,gy,np};
  adt->local = (float_layers){NEWA(float,ls*ls*np),ls,ls,np};
  printf("total adt size = %d + %d = %d\n",np*np,np*ls*ls,np*np+np*ls*ls);
  return adt;
}


/* Prepare a compressed version of many distance maps to allow for fast approximation of any pixel-to-pixel
   distance computation.
   Default values:
    step_local = 4
    step_global = 16
    max_iter = 40
    min_change = 0.1
*/
approx_dt_t* prepare_approx_dt( const float_image* cost, int step_global, int step_local, 
                                int hexagrid, dt_params_t* dt_params ) {
  const int tx = cost->tx;
  const int ty = cost->ty;
  DEFAULT_dt_params(dt_params)
  
  approx_dt_t* adt = _new_adt( tx, ty, step_global, step_local, hexagrid );
  float* global = adt->global.pixels;
  float* local = adt->local.pixels;
  const int gx = adt->global.tx;
  const int gy = adt->global.ty;
  const int np = adt->global.tz;
  const int ls = adt->local.tx;
  const int hs = ls/2;  // half
  
  #ifdef USE_OPENMP
  #pragma omp parallel
  #endif 
  {
    int n,i,j;
    float* tmp = NEWA(float,tx*ty); // one for each thread
    
    #ifdef USE_OPENMP
    #pragma omp for
    #endif 
    for(n=0; n<np; n++) {
      memset(tmp, 0x7F, tx*ty*sizeof(float));
      
      int_Tuple2 seed[2] = {n%gx, n/gx};
      tmp[hub_to_img(adt,seed[0],seed[1],seed)] = 0;
      
      float_image _tmp = {tmp,tx,ty};
      _weighted_distance_transform( cost, dt_params, &_tmp, NULL );
      
      // sample and copy result
      
      // local
      float* r = local + n*ls*ls;
      for(j=-hs; j<=hs; j++) 
        for(i=-hs; i<=hs; i++) {
          int x = seed[0] + i*step_local;  
          int y = seed[1] + j*step_local;
          BOUND_X_Y()
          IF_ASSERT(tmp[x+y*tx]>=0);
          *r++ = tmp[x+y*tx]; // no NaN values in local
        }
      
      // global
      r = global + n*np;
      for(j=0; j<gy; j++) 
        for(i=0; i<gx; i++) 
          *r++ = tmp[hub_to_img(adt,i,j,0)];
    }
    
    free(tmp);
  }
  
  return adt;
}


/* free the memory
*/
void free_approx_dt( approx_dt_t* adt ) {
  free(adt->global.pixels);
  free(adt->local.pixels);
  memset(adt,0,sizeof(*adt));
  free(adt);
}


static inline float get_local_distance( const approx_dt_t* adt,
                                        int hx, int hy, // hub point
                                        int x, int y ) // pixel 
{
  const int ls = adt->local.tx;
  const int hs = ls/2;
  const int gx = adt->global.tx;
  const int step_local = adt->step_local;
  
  float* local_map = adt->local.pixels + (hy*gx + hx)*ls*ls + ls*ls/2;
  IF_ASSERT(local_map[0]==0);
  
  // convert to relative pixel coordinates
  //int oldx=x, oldy=y, oldgx=hx, oldgy=hy;
  int_Tuple2 hp[2];
  hub_to_img(adt, hx, hy, hp);
  x -= hp[0];
  y -= hp[1];
  
  const int make_it_pos = hs*step_local;
  const int x0 = MIN((x+make_it_pos)/step_local-hs,hs-1);
  const int y0 = MIN((y+make_it_pos)/step_local-hs,hs-1);
  const float rx = (x - x0*step_local) / (float)step_local;
  const float ry = (y - y0*step_local) / (float)step_local;
  IF_ASSERT(rx>=0 && rx<=1 && ry>=0 && ry<=1);
  
  // affine interpolation from (x0,y0) ... (x0+1,y0+1)
  #define VALAT(x,y)  local_map[(x) + (y)*ls]
  float d0 = (1-rx)*VALAT(x0,y0  ) + rx*VALAT(x0+1,y0  );
  float d1 = (1-rx)*VALAT(x0,y0+1) + rx*VALAT(x0+1,y0+1);
  #undef VALAT
  float d = (1-ry)*d0 + ry*d1;
  IF_ASSERT(d>=0);
  return d;
}


/* parse the hub neighbours
*/
inline void hub_neighbours( const approx_dt_t* adt, int hx_0, int hy_0, int i, int* hx, int* hy ) {
  if(adt->hexagrid) {
    *hx = hx_0;
    *hy = hy_0;
    switch(i) {
    case 1: (*hx) += hy_0%2; (*hy)--; break;
    case 2: (*hx)++; break;
    case 3: (*hx) += hy_0%2; (*hy)++; break;
    }
  } else {
    *hx = hx_0 + (i%2);
    *hy = hy_0 + (i/2);
  }
  
  IF_ASSERT((*hx)>=0 && (*hx)<adt->global.tx);
  IF_ASSERT((*hy)>=0 && (*hy)<adt->global.ty);
}


/* Fast compute the approximate distance between two image pixels
   the distance is approximated as:
     min_(P1,P2)  d((x1,y1),P1) + d(P1,P2) + d(P2,(x2,y2)) 
     
     where P1, P2 are hub points (step_global) and 
       d((x,y),P) is approximated from the local sampling grid using 
       bilinear interpolation.
*/
float approx_dist( const approx_dt_t* adt, int x1, int y1, int x2, int y2 ) {
  assert(0<=x1 && x1<adt->tx);
  assert(0<=y1 && y1<adt->ty);
  assert(0<=x2 && x2<adt->tx);
  assert(0<=y2 && y2<adt->ty);
  const int np = adt->global.tx * adt->global.ty;
  assert( adt->global.tz == np );
  const int gx = adt->global.tx;
  const int gy = adt->global.ty;
  assert(gx>=2 && gy>=2);
  assert( adt->local.ty == adt->local.tx );
  
  // localize 4 hub points for pixel #1
  int hx1_0, hy1_0;
  img_to_topleft_hub(adt, x1, y1, &hx1_0, &hy1_0);
  
  // localize 4 hub points for pixel #2
  int hx2_0,hy2_0;
  img_to_topleft_hub(adt, x2, y2, &hx2_0, &hy2_0);
  
  // try different paths
  float best = 1e38;
  float d2[4] = {0};
  int i,j;
  for(i=0; i<4; i++) {
    int hx1,hy1;
    hub_neighbours(adt, hx1_0, hy1_0, i, &hx1, &hy1);
    
    // global distance map for seed=(hx1,hy1)
    float* global_map1 = adt->global.pixels + (hy1*gx + hx1)*np;
    
    // distance between (x1,y1) and hub point (hx1,hy1)
    float d1 = get_local_distance(adt, hx1, hy1, x1, y1 ); 
    
    for(j=0; j<4; j++) {
      int hx2,hy2;
      hub_neighbours(adt, hx2_0, hy2_0, j, &hx2, &hy2);
      
      if(!i) // compute once
        d2[j] = get_local_distance(adt, hx2, hy2, x2, y2 ); 
      
      float dist = d1 + global_map1[hx2 + hy2*gx] + d2[j];
      if( dist < best ) best=dist;
    }
  }
  
  return best;
}



/* return the position of nearest hub point
*/
void img_to_nearest_hub( const approx_dt_t* adt, int x, int y, int_Tuple2* res ) {
  int step = adt->step_global;
  if( adt->hexagrid ) {
    x += +step/2; // only difference
    int u = (x+y)/step;
    int v = x-y;v=v/adt->step_global-(v<0); // correct division
    y = (u-v);
    x = (u+v+(y%2))/2;
    y += 1;
  } else {
    x = (x+step/2)/step;
    y = (y+step/2)/step;
  }
  
  res[0] = x;
  res[1] = y;
//  return x + y*adt->global.tx;
}


/* Fast compute the distance map when the seed point is a hub point
    (hx,hy) : hub point in hub coordinates
*/
void _approx_dist_hub( const approx_dt_t* adt, int hx, int hy, float_image* res ) {
  const int tx = adt->tx;
  const int ty = adt->ty;
  assert(res->tx==tx && res->ty==ty);
  const int np = adt->global.tx * adt->global.ty;
  assert( adt->global.tz == np );
  const int gx = adt->global.tx;
  const int gy = adt->global.ty;
  assert(0<=hx && hx<gx);
  assert(0<=hy && hy<gy);
  assert(gx>=2 && gy>=2);
  const int ls = adt->local.tx;
  const int hs = ls/2;
  assert( adt->local.ty == ls );
  const int step_local = adt->step_local;
  assert(tx%step_local==0 && ty%step_local==0);
  const float inter = 1.f/step_local;
  
  // initialize with INF values
  float * r = res->pixels;
  memset(r, 0x7F, tx*ty*sizeof(float));
  
  // localize hub global map
  float* gmap = adt->global.pixels + (hx + hy*gx)*np;
  
  // we just paste all local dist-trf patches
  int n;
  for(n=0; n<np; n++) { // for all hub points
    float dist_offset = gmap[n];
    int_Tuple2 hp[2];
    hub_to_img(adt,n%gx,n/gx,hp);  // this hub point is at hp pixel coord
    
    float* local_map = adt->local.pixels + n*ls*ls + ls*ls/2;
    
    int i,j;
    const int minj =-MIN(    hp[1] /step_local, hs);  // hy + j*step_local >= 0
    const int maxj = MIN((ty-hp[1])/step_local, hs);  // hy + j*step_local < tx
    const int mini =-MIN(    hp[0] /step_local, hs);
    const int maxi = MIN((tx-hp[0])/step_local, hs);
    for(j=minj; j<maxj; j++) {
      int y = hp[1] + j*step_local;
      for(i=mini; i<maxi; i++) {
        int x = hp[0] + i*step_local;
        
        #define VALAT(x,y)  local_map[(x) + (y)*ls]
        float v00 = VALAT(i  ,j  );
        float v01 = VALAT(i  ,j+1);
        float v10 = VALAT(i+1,j  );
        float v11 = VALAT(i+1,j+1);
        #undef VALAT
        
        int u,v;
        float ry=0;
        for(v=0; v<step_local; v++,ry+=inter) {
          float rx=0;
          for(u=0; u<step_local; u++,rx+=inter) {
            float d0 = (1-rx)*v00 + rx*v10;
            float d1 = (1-rx)*v01 + rx*v11;
            float dist =  dist_offset + (1-ry)*d0 + ry*d1;
            
            float *R = r + (x+u) + (y+v)*tx;
            if( dist < *R ) *R = dist;
          }
        }
      }
    }
  }
}


inline float dist_lab( const float* a, const float* b ) {
  return sqrt(pow2(a[0]-b[0]) + pow2(a[1]-b[1]) + pow2(a[2]-b[2]));
}

/* Prepare a compressed version of many distance maps to allow for fast approximation of any pixel-to-pixel
   distance computation.
   Default values:
    step_local = 4
    step_global = 16
    int hexagrid = 0
*/
approx_dt_t* prepare_approx_color( const float_cube* lab, int step_global, int step_local, int hexagrid ) {
  const int tx = lab->tx;
  const int ty = lab->ty;
  assert(lab->tz==3);
  
  approx_dt_t* adt = _new_adt( tx, ty, step_global, step_local, hexagrid );
  float* global = adt->global.pixels;
  float* local = adt->local.pixels;
  const int gx = adt->global.tx;
  const int gy = adt->global.ty;
  const int np = adt->global.tz;
  const int ls = adt->local.tx;
  const int hs = ls/2;  // half
  
  int n;
/*  #ifdef USE_OPENMP*/
/*  #pragma omp parallel*/
/*  #endif */
  for(n=0; n<np; n++) {
    int i,j;
    
    int_Tuple2 seed[2] = {n%gx, n/gx};
    int h = hub_to_img(adt, seed[0], seed[1], seed);
    const float* hubcol = lab->pixels + 3*h;
    
    // sample and copy result
    
    // local
    float* r = local + n*ls*ls;
    for(j=-hs; j<=hs; j++) 
      for(i=-hs; i<=hs; i++) {
        int x = seed[0] + i*step_local;  
        int y = seed[1] + j*step_local;
        BOUND_X_Y()
        *r++ = dist_lab(hubcol, lab->pixels + 3*(x+y*tx)); // no NaN values in local
      }
    
    // global
    r = global + n*np;
    for(j=0; j<gy; j++) 
      for(i=0; i<gx; i++) 
        *r++ = dist_lab(hubcol, lab->pixels + 3*hub_to_img(adt,i,j,0));
  }
  
  return adt;
}


/* get maximum distance for this approx dt
*/
float get_dmax_approx_dt( const approx_dt_t* adt ) {
  return max_array_f( adt->global.pixels, adt->global.tx*adt->global.ty*adt->global.tz);
}

/* get mean distance for this approx dt
*/
float get_dmean_approx_dt( const approx_dt_t* adt ) {
  return mean_array_f( adt->global.pixels, adt->global.tx*adt->global.ty*adt->global.tz);
}


/* Normalize one approx_dt structure
*/
void mul_layers_f( float_layers* res, double mul ) {
  int n, nb = res->tx*res->ty*res->tz;
  for(n=0; n<nb; n++)
    res->pixels[n] *= mul;
}
void mul_approx_dt( approx_dt_t* res, double mul ) {
  mul_layers_f( &res->global, mul );
  mul_layers_f( &res->local , mul );
}


/* Add two approx_dt structure
*/
void add_layers_f( float_layers* res, const float_layers* add ) {
  int n, nb = res->tx*res->ty*res->tz;
  for(n=0; n<nb; n++)
    res->pixels[n] += add->pixels[n];
}
void add_approx_dt( approx_dt_t* res, const approx_dt_t* add ) {
  assert(memcmp(res,add,5*sizeof(int))==0);
  ASSERT_SAME_LAYERS_SIZE(&res->global, &add->global);
  ASSERT_SAME_LAYERS_SIZE(&res->local, &add->local);
  
  add_layers_f( &res->global, &add->global );
  add_layers_f( &res->local,  &add->local  );
}


/* Apply a sigmoid function (output is rescaled to [0,1])
*/
void sigmoid_layers_f( float_layers* res, float sigoff, float sigmul ) {
  int n, nb = res->tx*res->ty*res->tz;
  const float eoff = exp(sigoff*sigmul);
  
  for(n=0; n<nb; n++) {
    float e = exp( (res->pixels[n]+sigoff)*sigmul );
    res->pixels[n] = 1 - (1+eoff)/(1+e);
  }
}
void sigmoid_approx_dt( approx_dt_t* res, float sigoff, float sigmul ) {
  sigmoid_layers_f( &res->global, sigoff, sigmul );
  sigmoid_layers_f( &res->local , sigoff, sigmul );
}


































