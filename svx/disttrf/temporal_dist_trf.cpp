#include "temporal_dist_trf.h"
#include "std.h"



/* propagate the minimum distance along time
  matches = N x 6 = [(x0,y0, x1,y1, cost, useless)]
*/
float temporal_sweep( float_image* matches, 
                     float_image* dmap0, int_image* label0,
                     float_image* dmap1, int_image* label1 ) {
  ASSERT_SAME_SIZE(dmap0,label0);
  ASSERT_SAME_SIZE(dmap1,label1);
  ASSERT_SAME_SIZE(dmap0,dmap1);
  const int tx = dmap0->tx;
  assert(matches->tx==6);
  const int nm = matches->ty;
  
  float max_change = 0;
  
  for(int i=0; i<nm; i++) {
    float* m = matches->pixels + i*matches->tx;
    int u = int(m[0]+0.5) + int(m[1]+0.5)*tx; // pixel in first image
    int v = int(m[2]+0.5) + int(m[3]+0.5)*tx; // pixel in second image
    float cost = m[4];
    
    float* d0 = dmap0->pixels + u;
    float* d1 = dmap1->pixels + v;
    int* l0 = label0->pixels + u;
    int* l1 = label1->pixels + v;
    
    // try to propagate 0 --> 1
    float newdist = (*d0) + cost;
    if( newdist < *d1 ) {
      max_change = MAX(max_change, (*d1)-newdist );
      *d1 = newdist;
      *l1 = *l0;
    }
    
    // try to propagate 1 --> 0
    newdist = (*d1) + cost;
    if( newdist < *d0 ) {
      max_change = MAX(max_change, (*d0)-newdist );
      *d0 = newdist;
      *l0 = *l1;
    }
  }
  
  return max_change;
}




/* Compare two color distribution. The result is proportional to their similarity.
   seeds = [(x,y,L,a,b, nb, VL, Va, Vb), ...]
*/
void compare_colors( csr_matrix* ngh, const float_image* seeds, bool use_nb, int n_thread ) {
  const int ns = seeds->ty;
  const int sx = seeds->tx;
  assert(sx==6 || sx==9);
  assert(ngh->nr==ns && ngh->nc==ns);
  
  #ifdef USE_OPENMP
  #pragma omp parallel for num_threads(n_thread)
  #endif
  for(int i=0; i<ns; i++) {
    const float* si = seeds->pixels + i*sx;
    int ni = use_nb ? si[5] : 1;
    
    for(int j=ngh->indptr[i]; j<ngh->indptr[i+1]; j++) {
      const float* sj = seeds->pixels + ngh->indices[j]*sx;
      int nj = use_nb ? sj[5] : 1;
      
      float weight;
      if(sx==6) {
        weight = 0; // Lab distance
        for(int k=0; k<3; k++)
          weight += pow2(si[2+k]-sj[2+k]);
        
      } else {
        weight = 1; 
        for(int k=0; k<3; k++) {
          // new distribution has mean
          float mu = (si[2+k]*ni + sj[2+k]*nj)/(ni+nj);
          
          // and variance
          float var = (ni*(si[2+k]*si[2+k] + si[6+k]) + nj*(sj[2+k]*sj[2+k] + sj[6+k]))/(ni+nj) - mu*mu;
          
          // so the maximum of the distribution is in
          weight /= sqrt(MAX(1e-16,var));
        }
      }
      
      ngh->data[j] = weight;
    }
  }
}


















































