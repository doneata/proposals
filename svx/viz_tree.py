"""Visualization tools."""

import pdb

from matplotlib.pyplot import *
from numpy import *

from common import *

import colors as colutils
import disttrf
import flow_utils
import sparse_clustering as spclu
import sparse_graph_tools as sptools


ion()

CLICK_EVENT = False
CLICKED = []


def shuffled(labels):
    nl = labels.max() + 1
    random.seed(nl)
    order = arange(nl, dtype=int32)
    random.shuffle(order)
    return order[labels]


def recolored(labels, nodes):
    res = empty((3,) + labels.shape, dtype=float32)
    nodes = nodes.T[2:5]
    for i in range(3):
        res[i] = nodes[i][labels]
    return colutils.lab_to_rgb(res)


def combine_img_edge_weights(img, edw, mode=1):
    img = img.copy()
    edw = edw / edw.max()
    msk = (edw != 0)
    if mode == 0:
        img[msk, 1] = uint8(255 * 2 * maximum(0, 0.5 - edw[msk]))  # green
        img[msk, 2] = uint8(255 * 2 * (0.5 - abs(edw[msk] - 0.5)))  # blue
        img[msk, 0] = uint8(255 * 2 * maximum(0, edw[msk] - 0.5))  # red
    if mode == 1:
        img[msk, 2] = 0  # blue
        img[msk, 1] = uint8(255 * 2 * (0.5 - abs(edw[msk] - 0.5)))  # green
        img[msk, 0] = uint8(255 * 2 * maximum(0, edw[msk] - 0.5))  # red
    return img


def show_pairs(image, ngh, nodes, labels=None, subs=-5):
    coo = ngh.tocoo()
    print 'showing most %ssimilar pairs' % (subs < 0 and 'dis' or '')
    clf()
    if labels != None:
        ax2 = subplot(212)
        edw = disttrf.viz_edge_weights(ngh, labels)
        imshow(combine_img_edge_weights(zeros_like(image), edw),
               interpolation='nearest', vmin=0, cmap=cm.gray)
        ax = subplot(211)
        imshow(combine_img_edge_weights(image, edw))
    else:
        ax, ax2 = subplot(111), None
        imshow(image)
    for j in coo.data.argsort()[::subs]:
        ax.lines = []
        if ax2:
            ax2.lines = []
        pair = (coo.row[j], coo.col[j])
        print coo.data[j], pair, nodes[pair, :2].astype(int).ravel()
        ax.plot(nodes[pair, 0], nodes[pair, 1], '+',
                c='y', ms=10, mew=4, scalex=0, scaley=0)
        if ax2:
            ax2.plot(nodes[pair, 0], nodes[pair, 1], '+',
                     c='y', ms=10, mew=4, scalex=0, scaley=0)
        draw()
        inp = raw_input()
        if inp:
            break
    if inp in ('p', 'pdb'):
        pdb.set_trace()


def show_flow_mouse(im1, im2, flow):
    clf()
    ax1 = subplot(311)
    ax1.numplot = 0
    imshow(im1, interpolation='nearest')
    ax2 = subplot(312)
    ax2.numplot = 1
    imshow(im2, interpolation='nearest')
    ax3 = subplot(313)
    ax3.numplot = 2
    imshow(flow_utils.flowToColor(flow, maxmaxflow=200))

    def motion_notify_callback(event):
        if event.inaxes == None:
            return
        x, y = event.xdata, event.ydata
        ax1.lines = []
        ax1.artists = []
        ax2.lines = []
        ax2.artists = []
        ax3.lines = []
        ax3.artists = []
        npl = event.inaxes.numplot

        u, v = flow[int(y), int(x)]
        ax1.plot(
            [x], [y], '+', c='b', ms=10, mew=2, scalex=False, scaley=False)
        ax2.plot([x + u], [y + v], '+', c='b', ms=10,
                 mew=2, scalex=False, scaley=False)
        ax3.arrow(x, y, u, v, width=0, head_width=5)
        # we redraw only the concerned axes
        renderer = fig.canvas.get_renderer()
        ax1.draw(renderer)
        ax2.draw(renderer)
        ax3.draw(renderer)
        fig.canvas.blit(ax1.bbox)
        fig.canvas.blit(ax2.bbox)
        fig.canvas.blit(ax3.bbox)

    fig = get_current_fig_manager().canvas.figure
    cid_move = fig.canvas.mpl_connect(
        'motion_notify_event', motion_notify_callback)
    pdb.set_trace()
    fig.canvas.mpl_disconnect(cid_move)


def show_matches_mouse(im1, im2, corres):
    clf()
    ax1 = subplot(211)
    ax1.numplot = 0
    imshow(im1, interpolation='nearest')
    plot(corres[:, 0], corres[:, 1], '+',
         ms=10, c=(0, 1, 0), scalex=0, scaley=0)
    ax2 = subplot(212)
    ax2.numplot = 1
    imshow(im2, interpolation='nearest')
    plot(corres[:, 2], corres[:, 3], '+',
         ms=10, c=(0, 1, 0), scalex=0, scaley=0)

    def motion_notify_callback(event):
        if event.inaxes == None:
            return
        x, y = event.xdata, event.ydata
        ax1.lines = []
        ax1.artists = []
        ax2.lines = []
        ax2.artists = []
        npl = event.inaxes.numplot
        # find nearest point
        n = sum((corres[:, 2 * npl:2 * npl + 2] - [x, y]) ** 2, 1).argmin()
        print '\rcorres[%s] = %g' % (tuple(corres[n, :4]), corres[n, 4])
        ax1.plot(corres[n, 0], corres[n, 1], '+', c='b',
                 ms=10, mew=2, scalex=False, scaley=False)
        ax2.plot(corres[n, 2], corres[n, 3], '+', c='b',
                 ms=10, mew=2, scalex=False, scaley=False)
        # we redraw only the concerned axes
        renderer = fig.canvas.get_renderer()
        ax1.draw(renderer)
        ax2.draw(renderer)
        fig.canvas.blit(ax1.bbox)
        fig.canvas.blit(ax2.bbox)

    fig = get_current_fig_manager().canvas.figure
    cid_move = fig.canvas.mpl_connect(
        'motion_notify_event', motion_notify_callback)
    pdb.set_trace()
    fig.canvas.mpl_disconnect(cid_move)


def find(fr, x, y, final_labels, labels):
    res = labels[fr, int(y), int(x)]
    print
    print "Superpixel in frame %d at pos (%d, %d) = %d   ==> merged in %d" % (fr, int(x), int(y), res, final_labels[res])
    return res


def study_potential_merge(graph, tree, n_node, final_labels, u, v, verbose=0):
    cut_ngh = graph.tocoo()
    msk = (final_labels[cut_ngh.row] == u) & (final_labels[cut_ngh.col] == v)
    msk |= (final_labels[cut_ngh.row] == v) & (final_labels[cut_ngh.col] == u)
    cut_ngh.data[~msk] = 0
    cut_ngh.data[msk] += 1e-16  # Don't eliminate them.
    cut_ngh = cut_ngh.tocsr()
    cut_ngh.eliminate_zeros()
    assert cut_ngh.nnz, pdb.set_trace()
    if verbose:
        print 'Cost of next merge = %g (edges min/average/max: %g/%g/%g)' % (
            tree[:, 2].view(float32)[n_node - n_clusters],
            cut_ngh.data.min(),
            cut_ngh.data.mean(),
            cut_ngh.data.max())
    return cut_ngh


def display(
    data, nc, final_labels, true_colors, show_cut, study_func, start_end,
    xlist, pluslist, verbose=0):

    global n_clusters
    TO_SHUFFLE = 1
    n_clusters = nc

    def prepare_imrage(start_end):

        a, b = start_end
        n = len(data.labels)

        a = 1 if a is None else min(max(1, a), n)
        b = n if b is None else min(max(a, b), n)

        return range(a - 1, b)

    if verbose:
        print 'Showing %d segments, cost of last merge = %g.' % (n_clusters, data.tree[:, 2].view(float32)[data.n_node - 1 - n_clusters])

    # Just to visualize better.
    if TO_SHUFFLE:
        shuf = arange(n_clusters, dtype=int32)
        random.shuffle(shuf)
        final_labels = shuf[final_labels]

    if show_cut:
        # Those two will be merged next.
        u, v = final_labels[data.tree[data.n_node - n_clusters, :2]]
        cut_ngh = study_potential_merge(data.graph, data.tree, data.n_node, final_labels, u, v)

    clf()
    title("Supervoxels for n_clusters = %d" % n_clusters)

    imrange = prepare_imrage(start_end)
    nimg = len(imrange)

    c = int(sqrt(nimg))
    r = (nimg + c - 1) / c

    for i in imrange:
        ax = subplot(r, c, 1 + i - imrange[0])
        title('Frame %d' % (i + 1))
        ax.frame_num = i
        if true_colors:
            imshow(recolored(data.labels[i], data.all_nodes), vmin=0, vmax=n_clusters - 1, interpolation='nearest')
        elif show_cut:
            edw = disttrf.viz_edge_weights(cut_ngh, data.labels[i])
            imshow(edw, vmin=0, vmax=cut_ngh.data.max(), interpolation='nearest')
        else:
            imshow(final_labels[data.labels[i]], vmin=0, vmax=n_clusters - 1, interpolation='nearest')
        for node in xlist:
            if data.node_frames[node] == i:
                x, y = data.all_nodes[node, :2]
                plot(x, y, 'x', ms=20, c=(0, 0, 0), scalex=0, scaley=0)
        prev = (-10, )
        for p, node in enumerate(pluslist):
            if data.node_frames[node] == i:
                x, y = data.all_nodes[node, :2]
                if prev and prev[0] + 1 == p:
                    plot((prev[1], x), (prev[2], y), '-+', ms=10, c=(0, 0, 0), scalex=0, scaley=0)
                else:
                    plot(x, y, '+', ms=10, c=(0, 0, 0), scalex=0, scaley=0)
                prev = (p, x, y)

    def button_press_callback(event):
        if event.inaxes == None:
            return
        x, y = event.xdata, event.ydata
        l = find(event.inaxes.frame_num, x, y, final_labels, data.labels)
        global CLICKED
        if len(CLICKED) < 2:
            CLICKED.append(l)
        if len(CLICKED) == 2:
            if study_func == 'try_merge':
                try_merge(data.graph, data.tree, data.n_node, data.labels, imrange, final_labels, *CLICKED)
            elif study_func == 'track_merge':
                nc_, final_, xlist_, pluslist_ = track_merge(data.graph, data.tree, data.n_node, *CLICKED, verbose=verbose)
                display(
                    data, nc_, final_, true_colors, show_cut, study_func,
                    start_end, xlist_, pluslist_, verbose)
            CLICKED = []

    fig = get_current_fig_manager().canvas.figure

    global CLICK_EVENT
    if CLICK_EVENT:
        fig.canvas.mpl_disconnect(CLICK_EVENT)
    CLICK_EVENT = fig.canvas.mpl_connect('button_press_event', button_press_callback)


def track_merge(graph, tree, n_node, u, v, verbose=0):

    if verbose:
        print "Understanding fusion between node %d and %d." % (u, v)

    dis, path = sptools.shortest_path(graph, int(u), int(v), output_path=1)

    if verbose:
        print "Shortest path has distance %g." % dis

    # Find at which momment 2 nodes where united.
    i = 1
    j = len(tree)
    while i + 1 < j:
        mid = (i + j) / 2
        n_clusters, final = spclu.tree_assign(mid, n_node, tree)
        if final[u] == final[v]:
            i = mid
        else:
            j = mid
    cut = n_node - 1 - i

    if verbose:
        print "Merged at n_clusters=[%d --> %d] with score tree[%d,2] = %g" % (i, j, cut, tree[:, 2].view(float32)[cut])

    _, next_cut = spclu.tree_assign(i, n_node, tree)
    n_cluster, final = spclu.tree_assign(j, n_node, tree)

    assert next_cut[u] == next_cut[v], pdb.set_trace()
    assert final[u] != final[v]

    xlist = (u, v)
    pluslist = path

    return n_cluster, final, xlist, pluslist


def try_merge(graph, tree, n_node, labels, imrange, final_labels, u, v):

    # Translate node # to actual labelling.
    u = final_labels[u]
    v = final_labels[v]

    # Study the potential merge of u and v at <final_labels> state.
    cut_ngh = study_potential_merge(graph, tree, n_node, final_labels, u, v)

    # Display edges strength.
    for i in imrange:
        edw = disttrf.viz_edge_weights(cut_ngh, labels[i])
        imshow(edw, vmin=0, vmax=cut_ngh.data.max(), interpolation='nearest')


def showonly_merge(u, v):
    display(3, where(final_labels == final_labels[u], 1, where(final_labels == final_labels[v], 2, 0)))


def showonly(u, v, cut=0):
    if cut > len(tree):
        print 'cut cannot be higher than %d' % len(tree)
    n_clusters, final_labels = spclu.tree_assign(
        max(1, len(tree) - cut), n_node, tree)
    fl = zeros_like(final_labels)
    fl[final_labels == final_labels[u]] = 1
    fl[final_labels == final_labels[v]] = 2
    print 'edge_dist = ', graph[(fl == 1).nonzero()[0]][:, (fl == 2).nonzero()[0]].data.mean()
    display(3, fl, shuf=0)


def viz_tree(n_node, tree, display_func, verbose=0):

    imrange = (None, None)
    study_func = 'track_merge'

    if verbose:
        print 'Showing results.'

    global n_clusters
    n_clusters = 20

    subplots_adjust(left=0, right=1, top=1, bottom=0)

    true_colors = False
    rm_small_clusters = False
    show_cut = False

    while n_clusters:

        if rm_small_clusters:
            n_clusters, final_labels = remove_small_clusters(n_clusters, n_node, tree, th=rm_small_clusters)
            rm_small_clusters = False
        else:
            n_clusters, final_labels = spclu.tree_assign(n_clusters, n_node, tree)

        if verbose:
            print 'Actually, only %d real clusters.' % sum(bincount(final_labels, minlength=n_clusters) > 2)

        display_func(
            n_clusters=n_clusters,
            final_labels=final_labels,
            true_colors=true_colors,
            show_cut=show_cut,
            study_func=study_func,
            imrange=imrange,
        )

        print 'What number of clusters do you want to see?',
        inp = raw_input()

        true_colors = False
        show_cut = False

        if inp == '':
            pass
        elif inp in (' ', 't'):
            true_colors = True
        elif '-' in inp:
            imrange = map(int, inp.split('-'))
        elif inp in ('e', 'c'):
            show_cut = True
        elif inp.startswith('rm'):
            if len(inp) > 2:
                rm_small_clusters = int(inp[2:])
            else:
                rm_small_clusters = 2
        elif inp == 'k':
            study_func = 'track_merge'
            if verbose: print 'Changed mode to: track merge'
        elif inp == 'y':
            study_func = 'try_merge'
            if verbose: print 'Changed mode to: try merge'
        elif inp in ('p', 'pdb'):
            pdb.set_trace()
        elif inp in 'q':
            return
        else:
            n_clusters = int(inp)

