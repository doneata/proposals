
import argparse
import itertools
import numpy as np
import os
import random
import pdb
import sys

import matplotlib
import matplotlib.pyplot as plt

from PIL import Image

from proposals import load_proposals

sys.path.append('../svx/')
from datasets import DATASETS


def load_images(dataset, video):
    images_dir = dataset.get_images_path(video)
    images_paths = [os.path.join(images_dir, f) for f in os.listdir(images_dir)]
    return np.vstack([np.array(Image.open(ip))[np.newaxis] for ip in sorted(images_paths)])


def draw_rectangle(ax, xlow, ylow, xhigh, yhigh, color):
        ax.plot(
            [xlow, xlow, xhigh, xhigh, xlow],
            [ylow, yhigh, yhigh, ylow, ylow],
            color=color, linewidth=3)


def show_proposal(images, proposal, nr_rows, nr_cols, output_path):

    nr_frames = len(images)

    for ii, (row, col) in enumerate(itertools.product(xrange(nr_rows), xrange(nr_cols))):

        if ii >= nr_frames:
            break

        ax = plt.subplot2grid((nr_rows, nr_cols), (row, col))
        ax.set_axis_off()

        ax.imshow(images[ii])
        draw_rectangle(ax, *proposal[ii], color='red')

    if output_path:
        plt.savefig(output_path)
    else:
        plt.tight_layout()
        plt.show()


def subsample(xs, m):
    # Chooses `m` elements uniformly from a list `xs`.
    # See http://stackoverflow.com/a/9873804/474311
    n = len(xs)
    idxs = [i * n // m + n // (2 * m) for i in range(m)]
    return [xs[i] for i in idxs]


def factor(x):
    # Finds the two closest integers which, when multiplied, equal the input `x`.
    # See http://stackoverflow.com/a/16267018/474311
    n = int(np.floor(np.sqrt(x)))
    while True:
        if x % n == 0:
            return n, x / n
        n -= 1


def main():

    parser = argparse.ArgumentParser(description="Displays proposals onto the video frames.")

    parser.add_argument('--video', help="the name of the video to work on.")
    parser.add_argument('-d', '--dataset', choices=DATASETS.keys(), required=True, help="the name of the dataset.")
    parser.add_argument('-p', '--proposals', required=True, help="path to the proposal file.")
    parser.add_argument('-f', '--nr_frames', type=int, default=16, help="number of frames to show the proposal on.")
    parser.add_argument('-n', '--nr_proposals', type=int, default=5, help="how many proposals to show.")
    parser.add_argument('-o', '--outpath', default='', help="directory where to store the images showing the proposals.")
    parser.add_argument('--format', default='np', choices=('np', 'mat'), help="the format of the output (NumPy `np` or Matlab `mat`).")
    parser.add_argument('-v', '--verbose', action='count', help="verbosity level.")

    args = parser.parse_args()

    dataset = DATASETS[args.dataset]
    images = load_images(dataset, args.video)
    proposals = load_proposals(args.proposals, args.format).reshape(-1, images.shape[0], 4)

    nr_rows, nr_cols = factor(args.nr_frames)

    def prepare_path(path, ii):
        if path:
            return os.path.join(path, '%02d.png' % ii)
        else:
            return ''
    
    for ii, proposal in enumerate(random.sample(proposals, args.nr_proposals)):
        ss_images = subsample(images, args.nr_frames)
        ss_proposal = subsample(proposal, args.nr_frames)
        show_proposal(ss_images, ss_proposal, nr_rows, nr_cols, output_path=prepare_path(args.outpath, ii))


if __name__ == '__main__':
    main()


